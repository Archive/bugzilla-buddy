/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

#undef PACKAGE
#undef VERSION
#undef HAVE_LIBSM
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef GETTEXT_PACKAGE
#undef GNOME_EXPLICIT_TRANSLATION_DOMAIN
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef ENABLE_NLS
#undef SCRIPTS_DIR
