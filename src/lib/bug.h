/* 
 * gnome-bugzilla
 * Copyright (C) 2003 Christian Kellner
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 *
 * Filename:        $Source$
 * Date:          	$Date$ 
 * Revision:		$Revision$
 * Author:        	$Author$
 * Description:   	the (*phear*) bug object [header]
 *
 */


#ifndef GBZ_BUG_H
#define GBZ_BUG_H

#include <gnome.h>

G_BEGIN_DECLS

#define GBZ_TYPE_BUG		(gbz_bug_get_type ())
#define GBZ_BUG(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), GBZ_TYPE_BUG, GbzBug))
#define GBZ_BUG_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), GBZ_TYPE_BUG, GbzBugClass))
#define GBZ_IS_BUG(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GBZ_TYPE_BUG))
#define GBZ_IS_BUG_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GBZ_TYPE_BUG))

typedef struct _GbzBug			GbzBug;
typedef struct _GbzBugClass		GbzBugClass;

struct _GbzBugClass {
	GObjectClass  __parent_class;
	
};


struct _GbzBug {
	GObject 	__parent;
	
};


GType      gbz_bug_get_type (void);
GObject	  *gbz_bug_new      (void);

G_END_DECLS

#endif /* GBZ_BUG_H */
