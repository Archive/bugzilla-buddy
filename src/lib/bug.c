/* 
 * bugzilla-buddy
 * Copyright (C) 2003 Christian Kellner
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 * Filename:        $Source$
 * Date:          	$Date$ 
 * Revision:		$Revision$
 * Author:        	$Author$
 * Description:   	the (*phear*) bug object [implementation]
 */
 
#include <bug.h>

static GObjectClass *bug_parent_class = NULL;

/* Prototypes. */

static void gbz_bug_class_init (GbzBugClass *klass);
static void gbz_bug_finalize (GObject *object);
static void gbz_bug_instance_init (GbzBug *bug);

/* GObject Stuff */

static void
gbz_bug_class_init (GbzBugClass *klass)
{
	GObjectClass *object_class;

	object_class 	 = (GObjectClass *) klass;
	bug_parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_bug_finalize;
}

static void
gbz_bug_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (bug_parent_class)->finalize)
		G_OBJECT_CLASS (bug_parent_class)->finalize (object);
}

static void 
gbz_bug_instance_init (GbzBug *bug)
{

	
}


GType gbz_bug_get_type ()
{
        static guint type = 0;

        if (! type) {
            GTypeInfo type_info = {
			sizeof (GbzBugClass),
			NULL,	/* base_init */
			NULL,	/* base_finalize */
			(GClassInitFunc) gbz_bug_class_init,
			NULL,
			NULL,
			sizeof (GbzBug),
			0,
			(GInstanceInitFunc) gbz_bug_instance_init
		};

		type = g_type_register_static (G_TYPE_OBJECT,
					       "GbzBug",
					       &type_info,
					       0);
	}

        return (type);
}
