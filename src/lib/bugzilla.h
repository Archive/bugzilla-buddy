/* 
 * gnome-bugzilla
 * Copyright (C) 2003 Christian Kellner
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 *
 * Filename:        $Source$
 * Date:          	$Date$ 
 * Revision:		$Revision$
 * Author:        	$Author$
 * Description:   	bugzilla-bts object [header]
 *
 */


#ifndef GBZ_BUGZILLA_H
#define GBZ_BUGZILLA_H

#include <gnome.h>
#include <libgnomevfs/gnome-vfs-uri.h>

G_BEGIN_DECLS

#define GBZ_TYPE_BUGZILLA		 (gbz_bugzilla_get_type ())
#define GBZ_BUGZILLA(o)			 (G_TYPE_CHECK_INSTANCE_CAST ((o), GBZ_TYPE_BUG, GbzBugzilla))
#define GBZ_BUGZILLA_CLASS(k)	 (G_TYPE_CHECK_CLASS_CAST((k), GBZ_TYPE_BUG, GbzBugzillaClass))
#define GBZ_IS_BUGZILLA(o)		 (G_TYPE_CHECK_INSTANCE_TYPE ((o), GBZ_TYPE_BUGZILLA))
#define GBZ_IS_BUGZILLA_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GBZ_TYPE_BUGZILLA))

typedef struct _GbzBugzilla			 GbzBugzilla;
typedef struct _GbzBugzillaClass	 GbzBugzillaClass;

typedef struct _GbzBugzillaProduct	 GbzBugzillaProduct;
typedef struct _GbzBugzillaComponent GbzBugzillaComponent;

struct _GbzBugzillaClass {
	GObjectClass  __parent_class;
	
};


struct _GbzBugzilla {
	GObject 	__parent;
	
	char 		*name;
	GnomeVFSURI *uri;
	
	GdkPixbuf 	*icon;
	
};

struct _GbzBugzillaProduct {
	
	
	char *name;
	
};


struct _GbzBugzillaComponent {
	
	char *name;
	char *description;
	
	
};

GType      gbz_bugzilla_get_type (void);
GObject	  *gbz_bugzilla_new      (void);

G_END_DECLS

#endif /* GBZ_BUG_H */
