/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <bonobo.h>
#include <gconf/gconf-client.h>
#include <libgnome/gnome-macros.h>
#include <libgnome/gnome-util.h>
#include <libgnomeui/libgnomeui.h>
#include "window.h"
#include "GNOME_Bugzilla.h"

#define GBZ_BASE_KEY			"/apps/bugzilla-buddy/"
#define GBZ_PREF_WINDOW_WIDTH		"window_width"
#define GBZ_PREF_WINDOW_HEIGHT		"window_height"
#define GBZ_PREF_WINDOW_MAXIMIZED	"window_maximized"
#define GBZ_PREF_PANED_POSITION		"paned_position"

typedef struct _GbzWindowState GbzWindowState;

struct _GbzWindowState {
	int		width;
	int		height;
	int		paned_position;
	gboolean	maximized;
};

struct _GbzWindowPrivate {
	GtkWidget	*paned;
	GtkWidget	*bug_view;
	GtkWidget	*folder_tree;

	GNOME_Bugzilla_FolderTree folder_tree_object;

	GConfClient	*gconf_client;
};

/* Prototypes. */
static void gbz_window_class_init (GbzWindowClass *klass);
static void gbz_window_instance_init (GbzWindow *window);
static void gbz_window_finalize (GObject *object);
static void gbz_window_destroy (GtkWidget *widget, gpointer data);

static gboolean window_state_cb (GtkWidget *widget,
				 GdkEvent  *event,
				 gpointer   user_data);

static gboolean update_paned_cb (GtkWidget     *widget,
				 GtkAllocation *allocation,
				 gpointer       user_data);

static void load_state (GbzWindow *window);
static void save_state (GbzWindow *window);

static void exit_cmd (GtkWidget *widget, gpointer data);
static void about_cmd (GtkWidget *widget, gpointer data);

/* Menu verbs. */
static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("FileExit", exit_cmd),
	BONOBO_UI_UNSAFE_VERB ("HelpAbout", about_cmd),
	BONOBO_UI_VERB_END
};

/* Boilerplate. */
GNOME_CLASS_BOILERPLATE (GbzWindow, gbz_window,
			 BonoboWindow, BONOBO_TYPE_WINDOW);

/* Private methods. */
static void
gbz_window_class_init (GbzWindowClass *klass)
{
	GObjectClass *object_class;

	object_class = (GObjectClass *) klass;
	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_window_finalize;
}

static void
gbz_window_instance_init (GbzWindow *window)
{
	GbzWindowPrivate *priv;
	BonoboUIContainer *ui_container;
	BonoboControlFrame *frame;
	Bonobo_Control view;
	Bonobo_Control control;
	CORBA_Environment ev;

	priv = g_new0 (GbzWindowPrivate, 1);
	window->priv = priv;

	priv->gconf_client = gconf_client_get_default ();

	ui_container = bonobo_window_get_ui_container (BONOBO_WINDOW (window));

	gtk_window_set_resizable (GTK_WINDOW (window), TRUE);
	gtk_widget_realize (GTK_WIDGET (window));

	window->uic = bonobo_ui_component_new_default ();

	bonobo_ui_component_set_container (window->uic, 
					   BONOBO_OBJREF (ui_container), 
					   NULL);

	bonobo_ui_component_add_verb_list_with_data (window->uic, 
						     verbs, window);

	bonobo_ui_util_set_ui (window->uic, DATADIR, "bugzilla-buddy.xml",
			       "bugzilla-buddy", NULL);

	window->ui_container = ui_container;

	bonobo_ui_engine_config_set_path (bonobo_window_get_ui_engine (BONOBO_WINDOW (window)),
					  "/apps/bugzilla-buddy/uiconf/kvps");

	priv->paned = gtk_hpaned_new ();
	bonobo_window_set_contents (BONOBO_WINDOW (window), priv->paned);

	g_signal_connect (window, "window-state-event",
			  G_CALLBACK (window_state_cb), NULL);
	g_signal_connect (window, "configure-event",
			  G_CALLBACK (window_state_cb), NULL);
	g_signal_connect (window, "destroy",
			  G_CALLBACK (gbz_window_destroy), NULL);
	g_signal_connect (window, "delete_event",
			  G_CALLBACK (bonobo_main_quit), NULL);

	CORBA_exception_init (&ev);

	/* Create FolderTree control. */
	priv->folder_tree_object = bonobo_get_object ("OAFIID:GNOME_Bugzilla_FolderTree",
						      "IDL:GNOME/Bugzilla/FolderTree:1.0",
						      &ev);
	g_assert (priv->folder_tree_object != CORBA_OBJECT_NIL && !BONOBO_EX (&ev));
	control = Bonobo_Unknown_queryInterface (priv->folder_tree_object,
						 "IDL:Bonobo/Control:1.0",
						 &ev);
	g_assert (control != CORBA_OBJECT_NIL && !BONOBO_EX (&ev));
	priv->folder_tree = bonobo_widget_new_control_from_objref (control,
								   BONOBO_OBJREF (ui_container));
	frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (priv->folder_tree));
	bonobo_control_frame_set_autoactivate (frame, FALSE);
	gtk_widget_grab_focus (GTK_WIDGET (priv->folder_tree));
	bonobo_control_frame_control_activate (frame);
	g_signal_connect (priv->folder_tree, "size_allocate",
			  G_CALLBACK (update_paned_cb), window);

	/* Create BugView control. */
	view = GNOME_Bugzilla_FolderTree_getView (priv->folder_tree_object, &ev);
	g_assert (view != CORBA_OBJECT_NIL && !BONOBO_EX (&ev));
	priv->bug_view = bonobo_widget_new_control_from_objref (view,
								BONOBO_OBJREF (ui_container));
	frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (priv->bug_view));
	bonobo_control_frame_set_autoactivate (frame, FALSE);
	gtk_widget_grab_focus (GTK_WIDGET (priv->bug_view));
	bonobo_control_frame_control_activate (frame);

	CORBA_exception_free (&ev);

	gtk_paned_add1 (GTK_PANED (priv->paned), priv->folder_tree);
	gtk_paned_add2 (GTK_PANED (priv->paned), priv->bug_view);
	gtk_widget_show_all (priv->paned);

	load_state (window);
}

static void
gbz_window_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gbz_window_destroy (GtkWidget *widget, gpointer data)
{
	GbzWindow *window = GBZ_WINDOW (widget);

	save_state (window);

	g_object_unref (G_OBJECT (window->priv->gconf_client));
}

static gboolean
window_state_cb (GtkWidget *widget,
		 GdkEvent  *event,
		 gpointer   user_data)
{
	GbzWindowState *state;

	g_return_val_if_fail (widget != NULL, FALSE);

	state = g_object_get_data (G_OBJECT (widget), "window_state");
	if (!state) {
		state = g_new0 (GbzWindowState, 1);
		g_object_set_data (G_OBJECT (widget), "window_state", state);
	}

	switch (event->type) {
	case GDK_WINDOW_STATE:
		state->maximized = event->window_state.new_window_state &
			GDK_WINDOW_STATE_MAXIMIZED;
		break;
	case GDK_CONFIGURE:
		if (!state->maximized) {
			state->width = event->configure.width;
			state->height = event->configure.height;
		}
		break;
	default:
		break;
	}

	return FALSE;
}

static gboolean
update_paned_cb (GtkWidget     *widget,
		 GtkAllocation *allocation,
		 gpointer       user_data)
{
	GbzWindow *window = GBZ_WINDOW (user_data);
	GbzWindowState *state;

	state = g_object_get_data (G_OBJECT (window), "window_state");
	state->paned_position = gtk_paned_get_position (GTK_PANED (window->priv->paned));

	return FALSE;
}

static void
load_state (GbzWindow *window)
{
	GConfClient *gconf_client;
	GbzWindowState *state;

	state = g_object_get_data (G_OBJECT (window), "window_state");
	if (!state) {
		state = g_new0 (GbzWindowState, 1);
		g_object_set_data (G_OBJECT (window), "window_state", state);
	}

	/* restore window state */
	gconf_client = gconf_client_get_default ();
	state->width = gconf_client_get_int (gconf_client,
					     GBZ_BASE_KEY GBZ_PREF_WINDOW_WIDTH,
					     NULL);
	state->height = gconf_client_get_int (gconf_client,
					      GBZ_BASE_KEY GBZ_PREF_WINDOW_HEIGHT,
					      NULL);
	state->paned_position = gconf_client_get_int (gconf_client,
						      GBZ_BASE_KEY GBZ_PREF_PANED_POSITION,
						      NULL);
	state->maximized = gconf_client_get_bool (gconf_client,
						  GBZ_BASE_KEY GBZ_PREF_WINDOW_MAXIMIZED,
						  NULL);
	gtk_window_set_default_size (GTK_WINDOW (window), state->width, state->height);
	gtk_paned_set_position (GTK_PANED (window->priv->paned), state->paned_position);
	if (state->maximized)
		gtk_window_maximize (GTK_WINDOW (window));

	g_object_unref (gconf_client);

	GNOME_Bugzilla_FolderTree_loadAccounts (window->priv->folder_tree_object,
						"", NULL);
}

static void
save_state (GbzWindow *window)
{
	GConfClient *gconf_client;
	GbzWindowState *state;

	state = g_object_get_data (G_OBJECT (window), "window_state");
	if (!state)
		return;

	/* Save the window state. */
	gconf_client = gconf_client_get_default ();
	gconf_client_set_int (gconf_client,
			      GBZ_BASE_KEY GBZ_PREF_WINDOW_HEIGHT,
			      state->height,
			      NULL);
	gconf_client_set_int (gconf_client,
			      GBZ_BASE_KEY GBZ_PREF_WINDOW_WIDTH,
			      state->width,
			      NULL);
	gconf_client_set_int (gconf_client,
			      GBZ_BASE_KEY GBZ_PREF_PANED_POSITION,
			      state->paned_position,
			      NULL);
	gconf_client_set_bool (gconf_client,
			       GBZ_BASE_KEY GBZ_PREF_WINDOW_MAXIMIZED,
			       state->maximized,
			       NULL);
	g_object_unref (gconf_client);
}

static void
exit_cmd (GtkWidget *widget,
	  gpointer   data)
{
	GbzWindow *window  = GBZ_WINDOW (data);

	/* FIXME: accounts are only saved when user does File->Quit. */
	GNOME_Bugzilla_FolderTree_saveAccounts (window->priv->folder_tree_object,
						"", NULL);

	gtk_object_destroy (GTK_OBJECT (data));
	bonobo_main_quit ();
}

static void
about_cmd (GtkWidget *widget,
	   gpointer   data)
{
	static const char *authors[] = {
		"Jeroen Zwartepoorte",
		NULL};
	static GtkWidget *about = NULL;

	/* Convert names in the about box to utf8 */
	static gboolean converted = FALSE;
	if (!converted) {
		int i;
 		for (i = 0; i < G_N_ELEMENTS (authors) - 1; i++) {
			authors[i] = g_locale_to_utf8 (authors[i], -1,
						       NULL, NULL, NULL);
		}
		converted = TRUE;
	}

	if (!about) {
		about = gnome_about_new
			("bugzilla-buddy",
			 VERSION,
			 _("Copyright 2002 Jeroen Zwartepoorte"),
			 _("A Gnome frontend for bugzilla"), authors,
			 NULL, NULL, NULL);
		g_object_add_weak_pointer (G_OBJECT (about), &about);
		gtk_window_set_transient_for (GTK_WINDOW (about), GTK_WINDOW (data));
		gtk_widget_show (about);
	} else {
		gdk_window_raise (GTK_WIDGET (about)->window);
	}
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GtkWidget *
gbz_window_new (void)
{
	GbzWindow *window;

	window = GBZ_WINDOW (g_object_new (GBZ_TYPE_WINDOW,
					   "win_name", "bugzilla-buddy",
					   "title", "bugzilla-buddy",
					   NULL));

	return GTK_WIDGET (window);
}
