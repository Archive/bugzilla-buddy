/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include <libgnomeui/libgnomeui.h>
#include "window.h"

/* Main window. */
GbzWindow *main_window;

int main (int argc, char **argv)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	gnome_program_init ("gnome-bugzilla", VERSION, LIBGNOMEUI_MODULE, 
			    argc, argv, GNOME_PARAM_POPT_TABLE, 
			    bonobo_activation_popt_options, NULL);

	if (!bonobo_init (&argc, argv))
		g_error (_("Can't initialize bonobo!"));

	main_window = gbz_window_new ();

	gtk_widget_show_all (GTK_WIDGET (main_window));

	bonobo_main ();

	return 0;
}
