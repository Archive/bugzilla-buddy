/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef WINDOW_H
#define WINDOW_H

#include <bonobo.h>

G_BEGIN_DECLS

#define GBZ_TYPE_WINDOW		(gbz_window_get_type ())
#define GBZ_WINDOW(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GBZ_TYPE_WINDOW, GbzWindow))
#define GBZ_WINDOW_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), GBZ_TYPE_WINDOW, GbzWindowClass))
#define GBZ_IS_WINDOW(o)	(G_TYPE_CHECK_INSTANCE_TYPE ((o), GBZ_TYPE_WINDOW))
#define GBZ_IS_WINDOW_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GBZ_TYPE_WINDOW))

typedef struct _GbzWindow		GbzWindow;
typedef struct _GbzWindowPrivate	GbzWindowPrivate;
typedef struct _GbzWindowClass		GbzWindowClass;

struct _GbzWindow {
	BonoboWindow app;

	BonoboUIComponent *uic;
	BonoboUIContainer *ui_container;

	GbzWindowPrivate *priv;
};

struct _GbzWindowClass {
	BonoboWindowClass parent_class;
};


GType      gbz_window_get_type (void);
GtkWidget *gbz_window_new      (void);

G_END_DECLS

#endif /* WINDOW_H */
