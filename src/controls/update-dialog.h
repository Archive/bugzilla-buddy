/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_UPDATE_DIALOG_H
#define GBZ_UPDATE_DIALOG_H

#include <gtk/gtk.h>
#include "tree.h"

G_BEGIN_DECLS

#define GBZ_TYPE_UPDATE_DIALOG			(gbz_update_dialog_get_type ())
#define GBZ_UPDATE_DIALOG(obj)			(GTK_CHECK_CAST ((obj), GBZ_TYPE_UPDATE_DIALOG, GbzUpdateDialog))
#define GBZ_UPDATE_DIALOG_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GBZ_TYPE_UPDATE_DIALOG, GbzUpdateDialogClass))
#define GBZ_IS_UPDATE_DIALOG(obj)		(GTK_CHECK_TYPE ((obj), GBZ_TYPE_UPDATE_DIALOG))
#define GBZ_IS_UPDATE_DIALOG_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GBZ_TYPE_UPDATE_DIALOG))

typedef struct _GbzUpdateDialog			GbzUpdateDialog;
typedef struct _GbzUpdateDialogPrivate		GbzUpdateDialogPrivate;
typedef struct _GbzUpdateDialogClass		GbzUpdateDialogClass;

struct _GbzUpdateDialog {
	GtkDialog parent;

	GbzUpdateDialogPrivate *priv;
};

struct _GbzUpdateDialogClass {
	GtkDialogClass parent_class;
};

GType                     gbz_update_dialog_get_type (void);
GtkWidget                *gbz_update_dialog_new (GbzTree *tree);

void                      gbz_update_dialog_update (GbzUpdateDialog *dialog,
						    GSList          *accounts);

G_END_DECLS

#endif /* GBZ_UPDATE_DIALOG_H */
