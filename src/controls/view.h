/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_VIEW_H
#define GBZ_VIEW_H

#include <gtk/gtk.h>
#include "query.h"

G_BEGIN_DECLS

#define GBZ_TYPE_VIEW			(gbz_view_get_type ())
#define GBZ_VIEW(obj)			(GTK_CHECK_CAST ((obj), GBZ_TYPE_VIEW, GbzView))
#define GBZ_VIEW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GBZ_TYPE_VIEW, GbzViewClass))
#define GBZ_IS_VIEW(obj)		(GTK_CHECK_TYPE ((obj), GBZ_TYPE_VIEW))
#define GBZ_IS_VIEW_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GBZ_TYPE_VIEW))

typedef struct _GbzView			GbzView;
typedef struct _GbzViewPrivate		GbzViewPrivate;
typedef struct _GbzViewClass		GbzViewClass;

struct _GbzView {
	GtkVBox parent;

	GbzViewPrivate *priv;
};

struct _GbzViewClass {
	GtkVBoxClass parent_class;
};

GType                     gbz_view_get_type (void);
GtkWidget                *gbz_view_new (void);

void                      gbz_view_set_query (GbzView  *view,
					      GbzQuery *query);
GbzQuery                 *gbz_view_get_query (GbzView  *view);

G_END_DECLS

#endif /* GBZ_VIEW_H */
