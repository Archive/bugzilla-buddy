/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <bonobo.h>
#include <gconf/gconf-client.h>
#include <gtk/gtk.h>
#include "commands.h"
#include "account-druid.h"
#include "folder-druid.h"
#include "update-dialog.h"

void
new_account (BonoboUIComponent *uic,
	     BonoboControl     *control,
	     const char        *verb)
{
	GbzTree *tree;
	GbzAccountDruid *druid;

	tree = g_object_get_data (G_OBJECT (control), "GbzTree");
	druid = gbz_account_druid_new ();

	gbz_account_druid_new_account (druid, tree);
}

void
new_folder (BonoboUIComponent *uic,
	    BonoboControl     *control,
	    const char        *verb)
{
	GbzTree *tree;
	GtkWidget *druid;

	tree = g_object_get_data (G_OBJECT (control), "GbzTree");
	druid = gbz_folder_druid_new (tree);

#if 0
	bonobo_control_set_transient_for (control, GTK_WINDOW (druid), NULL);
#endif
	gtk_widget_show (druid);
}

void
update_products (BonoboUIComponent *uic,
		 BonoboControl     *control,
		 const char        *verb)
{
	GbzTree *tree;
	GtkWidget *dialog;

	tree = g_object_get_data (G_OBJECT (control), "GbzTree");

	dialog = gbz_update_dialog_new (tree);

#if 0
	bonobo_control_set_transient_for (control, GTK_WINDOW (dialog), NULL);
#endif
	gbz_update_dialog_update (GBZ_UPDATE_DIALOG (dialog),
				  gbz_tree_get_accounts (tree));
}
