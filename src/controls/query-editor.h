/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_QUERY_EDITOR_H
#define GBZ_QUERY_EDITOR_H

G_BEGIN_DECLS

#define GBZ_TYPE_QUERY_EDITOR			(gbz_query_editor_get_type ())
#define GBZ_QUERY_EDITOR(obj)			(GTK_CHECK_CAST ((obj), GBZ_TYPE_QUERY_EDITOR, GbzQueryEditor))
#define GBZ_QUERY_EDITOR_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GBZ_TYPE_QUERY_EDITOR, GbzQueryEditorClass))
#define GBZ_IS_QUERY_EDITOR(obj)		(GTK_CHECK_TYPE ((obj), GBZ_TYPE_QUERY_EDITOR))
#define GBZ_IS_QUERY_EDITOR_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GBZ_TYPE_QUERY_EDITOR))

typedef struct _GbzQueryEditor			GbzQueryEditor;
typedef struct _GbzQueryEditorPrivate		GbzQueryEditorPrivate;
typedef struct _GbzQueryEditorClass		GbzQueryEditorClass;

struct _GbzQueryEditor {
	GtkVBox parent;

	GbzQueryEditorPrivate *priv;
};

struct _GbzQueryEditorClass {
	GtkVBoxClass parent_class;
};

GType                     gbz_query_editor_get_type (void);
GbzQueryEditor           *gbz_query_editor_new (void);

G_END_DECLS

#endif /* GBZ_QUERY_EDITOR_H */
