/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gtk/gtk.h>
#include <libbonoboui.h>
#include "tree-object.h"

struct _GbzTreeObjectPrivate {
	GbzTree *tree;
};

static BonoboObjectClass *parent_class;

/* Prototypes. */
static void gbz_tree_object_class_init (GbzTreeObjectClass *klass);
static void gbz_tree_object_init (GbzTreeObject *treeobj);
static void gbz_tree_object_finalize (GObject *object);

static void impl_load_accounts (PortableServer_Servant servant,
				const CORBA_char      *filename,
				CORBA_Environment     *ev);
static void impl_save_accounts (PortableServer_Servant servant,
				const CORBA_char      *filename,
				CORBA_Environment     *ev);

static Bonobo_Control impl_get_view (PortableServer_Servant servant,
				     CORBA_Environment     *ev);

/* Private methods. */
static void
gbz_tree_object_class_init (GbzTreeObjectClass *klass)
{
	GObjectClass *object_class;
	POA_GNOME_Bugzilla_FolderTree__epv *epv = &klass->epv;

	object_class = (GObjectClass *) klass;
	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_tree_object_finalize;

	epv->loadAccounts = impl_load_accounts;
	epv->saveAccounts = impl_save_accounts;
	epv->getView = impl_get_view;
}

static void
gbz_tree_object_init (GbzTreeObject *treeobj)
{
	GbzTreeObjectPrivate *priv;

	priv = g_new0 (GbzTreeObjectPrivate, 1);
	treeobj->priv = priv;
}
static void
gbz_tree_object_finalize (GObject *object)
{
}

static void
impl_load_accounts (PortableServer_Servant servant,
		    const CORBA_char      *filename,
		    CORBA_Environment     *ev)
{
	GbzTreeObject *treeobj = GBZ_TREE_OBJECT (bonobo_object_from_servant (servant));

	gbz_tree_load_config (treeobj->priv->tree);
}

static void
impl_save_accounts (PortableServer_Servant servant,
		    const CORBA_char      *filename,
		    CORBA_Environment     *ev)
{
	GbzTreeObject *treeobj = GBZ_TREE_OBJECT (bonobo_object_from_servant (servant));

	gbz_tree_save_config (treeobj->priv->tree);
}

static Bonobo_Control
impl_get_view (PortableServer_Servant servant,
	       CORBA_Environment     *ev)
{
	GbzTreeObject *treeobj = GBZ_TREE_OBJECT (bonobo_object_from_servant (servant));
	GtkWidget *label;
	BonoboControl *view;

	label = gtk_label_new ("View");
	gtk_widget_show (label);
	view = bonobo_control_new (label);

	return BONOBO_OBJREF (view);
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GbzTreeObject *
gbz_tree_object_new (GbzTree *tree)
{	GbzTreeObject *treeobj;

	treeobj = GBZ_TREE_OBJECT (g_object_new (GBZ_TYPE_TREE_OBJECT, NULL));
	treeobj->priv->tree = tree;

	return treeobj;
}

BONOBO_TYPE_FUNC_FULL (GbzTreeObject,
		       GNOME_Bugzilla_FolderTree,
		       BONOBO_OBJECT_TYPE,
		       gbz_tree_object);
