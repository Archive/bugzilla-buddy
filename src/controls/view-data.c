/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "view-data.h"

GType
gbz_view_data_get_type (void)
{
	static GType our_type = 0;

	if (our_type == 0)
		our_type = g_boxed_type_register_static ("GbzProjectViewNodeData",
							 (GBoxedCopyFunc) gbz_view_data_copy,
							 (GBoxedFreeFunc) gbz_view_data_free);

	return our_type;
}

GbzViewData *
gbz_view_data_new (GbzViewNodeType type, gpointer data) 
{
	GbzViewData *node = g_new0 (GbzViewData, 1);

	node->type = type;
	node->data = data;

	return node;
}

GbzViewData *
gbz_view_data_copy (GbzViewData *src)
{
	GbzViewData *node;

	node = g_new (GbzViewData, 1);
	node->type = src->type;
	node->data = src->data;

	return node;
}

void
gbz_view_data_free (GbzViewData *node)
{
	//g_free (node->data);
	g_free (node);
}
