/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <libgnomeui/gnome-druid.h>
#include <libgnomeui/gnome-druid-page-standard.h>
#include <string.h>
#include "update-dialog.h"

enum {
	PROP_BOGUS,
	PROP_TREE
};

typedef struct _AccountPane AccountPane;

struct _AccountPane {
	GtkWidget	*status_image;
	GtkWidget	*status_label;
	GtkWidget	*progress_bar;
	GtkWidget	*cancel_button;
	GbzAccount	*account;
	GThread		*thread;
};

struct _GbzUpdateDialogPrivate {
	GbzTree		*tree;
	GSList		*account_panes;
	int		timeout;
};

/* Prototypes. */
static void gbz_update_dialog_get_property (GObject    *object,
					    guint       prop_id,
					    GValue     *value,
					    GParamSpec *pspec);
static void gbz_update_dialog_set_property (GObject      *object,
					    guint         prop_id,
					    const GValue *value,
					    GParamSpec   *pspec);
static void gbz_update_dialog_finalize (GObject *object);

static void add_accounts (GbzUpdateDialog *dialog,
			  GSList          *accounts);
static void update_accounts (GbzUpdateDialog *dialog);
static void update_account_thread (AccountPane *pane);
static gboolean timeout_cb (GbzUpdateDialog *dialog);

static void cancel_all_cb     (GtkButton *button,
			       gpointer   user_data);
static void cancel_account_cb (GtkButton *button,
			       gpointer   user_data);

static GtkWidget *new_button_with_stock_image (const char *text,
					       const char *stock_id);

/* Boilerplate code. */
GNOME_CLASS_BOILERPLATE (GbzUpdateDialog, gbz_update_dialog,
			 GtkDialog, GTK_TYPE_DIALOG);

/* Private routines */
static void
gbz_update_dialog_class_init (GbzUpdateDialogClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_update_dialog_finalize;
	object_class->get_property = gbz_update_dialog_get_property;
	object_class->set_property = gbz_update_dialog_set_property;

	g_object_class_install_property (object_class,
					 PROP_TREE,
					 g_param_spec_pointer ("tree",
							       _("Tree"),
							       _("Current tree."),
							       G_PARAM_READWRITE));
}

static void
gbz_update_dialog_instance_init (GbzUpdateDialog *dialog)
{
	GbzUpdateDialogPrivate *priv;
	GtkWidget *button;

	priv = g_new0 (GbzUpdateDialogPrivate, 1);
	dialog->priv = priv;

	gtk_window_set_title (GTK_WINDOW (dialog), _("Send & Retrieve Updates"));
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);

	button = new_button_with_stock_image (_("_Cancel All"), GTK_STOCK_CANCEL);
	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_widget_show (button);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_CANCEL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CANCEL);
	g_signal_connect (button, "clicked", G_CALLBACK (cancel_all_cb), dialog);
}

static void
gbz_update_dialog_get_property (GObject    *object,
				guint       prop_id,
				GValue     *value,
				GParamSpec *pspec)
{
	GbzUpdateDialog *dialog = GBZ_UPDATE_DIALOG (object);

	switch (prop_id) {
		case PROP_TREE:
			g_value_set_pointer (value, dialog->priv->tree);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
gbz_update_dialog_set_property (GObject      *object,
				guint         prop_id,
				const GValue *value,
				GParamSpec   *pspec)
{
	GbzUpdateDialog *dialog = GBZ_UPDATE_DIALOG (object);

	switch (prop_id) {
		case PROP_TREE:
			dialog->priv->tree = g_value_get_pointer (value);
			break;
		default:
			break;
	}
}

static void
gbz_update_dialog_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
cancel_all_cb (GtkButton *button,
	       gpointer   user_data)
{
	GbzUpdateDialog *dialog = GBZ_UPDATE_DIALOG (user_data);

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
cancel_account_cb (GtkButton *button,
		   gpointer   user_data)
{
}

static void
add_accounts (GbzUpdateDialog *dialog,
	      GSList          *accounts)
{
	GbzUpdateDialogPrivate *priv = dialog->priv;
	GtkWidget *table, *vbox, *label;
	char *bugzilla, *label_str;
	GSList *l;
	int i;
	AccountPane *pane;

	table = gtk_table_new (g_slist_length (accounts), 4, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);

	for (i = 0, l = accounts; l; i++, l = l->next) {
		GbzAccount *account = l->data;
		pane = g_new0 (AccountPane, 1);
		pane->account = account;

		/* Add status image. */
		pane->status_image = gtk_image_new_from_stock (GTK_STOCK_GO_DOWN,
							       GTK_ICON_SIZE_BUTTON);
		gtk_misc_set_alignment (GTK_MISC (pane->status_image), 0.0, 0.0);
		gtk_table_attach_defaults (GTK_TABLE (table), pane->status_image,
					   0, 1, i, i + 1);

		/* Add labels. */
		vbox = gtk_vbox_new (FALSE, 0);
		g_object_get (G_OBJECT (account), "bugzilla", &bugzilla, NULL);
		label_str = g_strconcat (_("Server: "), bugzilla, NULL);
		label = gtk_label_new (label_str);
		g_free (label_str);
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
		pane->status_label = gtk_label_new ("status...");
		gtk_misc_set_alignment (GTK_MISC (pane->status_label), 0.0, 0.0);
		gtk_box_pack_start (GTK_BOX (vbox), pane->status_label, FALSE, FALSE, 0);
		gtk_table_attach_defaults (GTK_TABLE (table), vbox,
					   1, 2, i, i + 1);

		/* Add progress bar. */
		vbox = gtk_vbox_new (TRUE, 0);
		pane->progress_bar = gtk_progress_bar_new ();
		gtk_box_pack_start (GTK_BOX (vbox), pane->progress_bar, FALSE, FALSE, 0);
		gtk_table_attach_defaults (GTK_TABLE (table), vbox,
					   2, 3, i, i + 1);

		/* Add cancel button. */
		pane->cancel_button = new_button_with_stock_image (_("Cancel"),
								   GTK_STOCK_CANCEL);
		g_signal_connect (G_OBJECT (pane->cancel_button), "clicked",
				  G_CALLBACK (cancel_account_cb), pane);
		gtk_table_attach_defaults (GTK_TABLE (table), pane->cancel_button,
					   3, 4, i, i + 1);

		priv->account_panes = g_slist_append (priv->account_panes, pane);
	}

	gtk_widget_show_all (table);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), table, TRUE,
			    TRUE, 0);
}

static void
update_accounts (GbzUpdateDialog *dialog)
{
	GSList *l;

	/* Create timeout callback to update progress bars. */
	dialog->priv->timeout = gtk_timeout_add (100,
						 (GSourceFunc)timeout_cb,
						 dialog);

	for (l = dialog->priv->account_panes; l; l = l->next) {
		AccountPane *pane = l->data;

		pane->thread = g_thread_create ((GThreadFunc) update_account_thread,
						 pane, TRUE, NULL);
	}
}

static gboolean
timeout_cb (GbzUpdateDialog *dialog)
{
	GSList *l;

	for (l = dialog->priv->account_panes; l; l = l->next) {
		AccountPane *pane = l->data;
	}

	return TRUE;
}

static void
update_account_thread (AccountPane *pane)
{
}

static GtkWidget *
new_button_with_stock_image (const char *text,
			     const char *stock_id)
{
	GtkWidget *button;
	GtkStockItem item;
	GtkWidget *label;
	GtkWidget *image;
	GtkWidget *hbox;
	GtkWidget *align;

	button = gtk_button_new ();

	if (GTK_BIN (button)->child)
		gtk_container_remove (GTK_CONTAINER (button),
				      GTK_BIN (button)->child);

	if (gtk_stock_lookup (stock_id, &item)) {
		label = gtk_label_new_with_mnemonic (text);

		gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));

		image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_BUTTON);
		hbox = gtk_hbox_new (FALSE, 2);

		align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);

		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
		gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);

		gtk_container_add (GTK_CONTAINER (button), align);
		gtk_container_add (GTK_CONTAINER (align), hbox);
		gtk_widget_show_all (align);

		return button;
	}

	label = gtk_label_new_with_mnemonic (text);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));

	gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);

	gtk_widget_show (label);
	gtk_container_add (GTK_CONTAINER (button), label);

	return button;
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GtkWidget *
gbz_update_dialog_new (GbzTree *tree)
{
	return GTK_WIDGET (g_object_new (GBZ_TYPE_UPDATE_DIALOG, "tree", tree,
					 NULL));
}

void
gbz_update_dialog_update (GbzUpdateDialog *dialog,
			  GSList          *accounts)
{
	add_accounts (dialog, accounts);

	gtk_widget_show (GTK_WIDGET (dialog));

	update_accounts (dialog);
}
