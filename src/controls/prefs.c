/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <glade/glade-xml.h>
#include "prefs.h"

void
gbz_show_prefs (void)
{	GladeXML *gui;
	GtkWidget *dialog, *server_entry, *email_entry, *password_entry;
	GtkResponseType response;
	GConfClient *client;

	/* Open gbz-view.glade. */
	gui = glade_xml_new (DATADIR "/bugzilla-buddy/glade/tree.glade", NULL, NULL);
	if (!gui) {
		g_warning ("Could not find tree.glade, reinstall bugzilla-buddy.\n");
		return 0;
	}

	dialog = glade_xml_get_widget (gui, "prefdialog");
	server_entry = glade_xml_get_widget (gui, "server-entry");
	email_entry = glade_xml_get_widget (gui, "email-entry");
	password_entry = glade_xml_get_widget (gui, "password-entry");

	client = gconf_client_get_default ();

	gtk_entry_set_text (GTK_ENTRY (server_entry),
			    gconf_client_get_string (client,
						     GBZ_BASE_KEY GBZ_SERVER_KEY,
						     NULL));
	gtk_entry_set_text (GTK_ENTRY (email_entry),
			    gconf_client_get_string (client,
						     GBZ_BASE_KEY GBZ_USERNAME_KEY,
						     NULL));
	gtk_entry_set_text (GTK_ENTRY (password_entry),
			    gconf_client_get_string (client,
						     GBZ_BASE_KEY GBZ_PASSWORD_KEY,
						     NULL));

	response = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_hide (dialog);

	gconf_client_set_string (client, GBZ_BASE_KEY GBZ_SERVER_KEY,
				 gtk_entry_get_text (GTK_ENTRY (server_entry)), NULL);
	gconf_client_set_string (client, GBZ_BASE_KEY GBZ_USERNAME_KEY,
				 gtk_entry_get_text (GTK_ENTRY (email_entry)), NULL);
	gconf_client_set_string (client, GBZ_BASE_KEY GBZ_PASSWORD_KEY,
				 gtk_entry_get_text (GTK_ENTRY (password_entry)), NULL);

	g_object_unref (G_OBJECT (client));
}