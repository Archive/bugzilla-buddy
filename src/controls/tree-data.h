/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_TREE_DATA_H
#define GBZ_TREE_DATA_H

#include <glib-object.h>

G_BEGIN_DECLS

#define GBF_TYPE_TREE_DATA	(gbz_tree_data_get_type ())

typedef struct _GbzTreeData GbzTreeData;

typedef enum {
	GBZ_TREE_NODE_ACCOUNT,
	GBZ_TREE_NODE_QUERY,
	GBZ_TREE_NODE_FOLDER
} GbzTreeNodeType;

struct _GbzTreeData {
	GbzTreeNodeType type;
	gpointer data;
};

GType        gbz_tree_data_get_type (void);
GbzTreeData *gbz_tree_data_new      (GbzTreeNodeType  type,
				     gpointer         data);
GbzTreeData *gbz_tree_data_copy     (GbzTreeData     *data);
void         gbz_tree_data_free     (GbzTreeData     *data);

G_END_DECLS

#endif /* GBZ_TREE_DATA_H */
