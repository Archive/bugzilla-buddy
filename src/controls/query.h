/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_QUERY_H
#define GBZ_QUERY_H

#include <gtk/gtkdialog.h>
#include "GNOME_Bugzilla.h"

G_BEGIN_DECLS

typedef struct _GbzQuery		GbzQuery;
typedef struct _GbzQueryPrivate		GbzQueryPrivate;
typedef struct _GbzQueryClass		GbzQueryClass;

#define GBZ_QUERY_TYPE        (gbz_query_get_type ())
#define GBZ_QUERY(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GBZ_QUERY_TYPE, GbzQuery))
#define GBZ_QUERY_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GBZ_QUERY_TYPE, GbzQueryClass))
#define GBZ_IS_QUERY(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GBZ_QUERY_TYPE))
#define GBZ_IS_QUERY_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GBZ_QUERY_TYPE))

struct _GbzQuery {
	GObject parent;

	GbzQueryPrivate *priv;
};

struct _GbzQueryClass {
	GObjectClass parent_class;
};

GType                       gbz_query_get_type (void);
GbzQuery                   *gbz_query_new (void);

GtkResponseType             gbz_query_show_dialog (GbzQuery *query);

G_END_DECLS

#endif /* GBZ_QUERY_H */
