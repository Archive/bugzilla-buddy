/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <libgnomeui/gnome-druid.h>
#include <libgnomeui/gnome-druid-page-standard.h>
#include <string.h>
#include "folder-druid.h"
#include "tree-data.h"
#include "query-editor.h"

enum {
	PROP_BOGUS,
	PROP_TREE
};

enum {
	TYPE_COMPONENT,
	TYPE_VERSION,
	TYPE_MILESTONE
};

static char *type_list[] = {
	"Plain",
	"Product",
	"Query",
	NULL
};

struct _GbzFolderDruidPrivate {
	/* Druid. */
	GnomeDruid		*druid;
	GnomeDruidPage		*folder_page;
	GnomeDruidPage		*plain_page;
	GnomeDruidPage		*product_page;
	GnomeDruidPage		*component_page;
	GnomeDruidPage		*query_page;

	/* Folder page. */
	GtkWidget		*type_option;
	GtkWidget		*folder_tree;
	GtkTreeModel		*folder_model;

	/* Plain page. */
	GtkWidget		*name_entry;

	/* Product page. */
	GtkWidget		*product_tree;
	GtkTreeModel		*product_model;
	GtkWidget		*refresh_hbox;
	GtkWidget		*refresh_button;

	/* Component page. */
	GtkWidget		*component_tree;
	GtkTreeModel		*component_model;

	/* Query page. */
	GbzQueryEditor		*query_editor;

	/* Miscellaneous. */
	GbzTree			*tree;
	GtkTreePath		*selected_path;
	GbzAccount		*selected_account;
	GbzProductInfo		*selected_product;
};

/* Prototypes. */
static void gbz_folder_druid_finalize (GObject *object);
static void gbz_folder_druid_get_property (GObject    *object,
					   guint       prop_id,
					   GValue     *value,
					   GParamSpec *pspec);
static void gbz_folder_druid_set_property (GObject      *object,
					   guint         prop_id,
					   const GValue *value,
					   GParamSpec   *pspec);

static void set_pixbuf (GtkTreeViewColumn *tree_column,
			GtkCellRenderer   *cell,
			GtkTreeModel      *model,
			GtkTreeIter       *iter,
			gpointer           data);

static void set_folder_text (GtkTreeViewColumn *tree_column,
			     GtkCellRenderer   *cell,
			     GtkTreeModel      *model,
			     GtkTreeIter       *iter,
			     gpointer           data);

static void set_toggle (GtkTreeViewColumn *tree_column,
			GtkCellRenderer   *cell,
			GtkTreeModel      *model,
			GtkTreeIter       *iter,
			gpointer           data);

static void set_component_text (GtkTreeViewColumn *tree_column,
				GtkCellRenderer   *cell,
				GtkTreeModel      *model,
				GtkTreeIter       *iter,
				gpointer           data);

static void cell_toggled_cb (GtkCellRendererToggle *renderer,
			     const char            *path,
			     gpointer               data);

static GnomeDruidPage *create_folder_page (GbzFolderDruid *druid);
static GnomeDruidPage *create_plain_page (GbzFolderDruid *druid);
static GnomeDruidPage *create_product_page (GbzFolderDruid *druid);
static GnomeDruidPage *create_component_page (GbzFolderDruid *druid);
static GnomeDruidPage *create_query_page (GbzFolderDruid *druid);

static gboolean folder_next_cb (GnomeDruidPage *page,
				GtkWidget      *widget,
				gpointer        data);
static gboolean plain_prepare_cb (GnomeDruidPage *page,
				  GtkWidget      *widget,
				  gpointer        data);
static gboolean plain_finish_cb (GnomeDruidPage *page,
				 GtkWidget      *widget,
				 gpointer        data);
static gboolean product_prepare_cb (GnomeDruidPage *page,
				    GtkWidget      *widget,
				    gpointer        data);
static gboolean product_next_cb (GnomeDruidPage *page,
				 GtkWidget      *widget,
				 gpointer        data);
static gboolean component_prepare_cb (GnomeDruidPage *page,
				      GtkWidget      *widget,
				      gpointer        data);
static gboolean component_finish_cb (GnomeDruidPage *page,
				     GtkWidget      *widget,
				     gpointer        data);

/* Boilerplate code. */
GNOME_CLASS_BOILERPLATE (GbzFolderDruid, gbz_folder_druid,
			 GtkWindow, GTK_TYPE_WINDOW);

/* Private routines */
static void
gbz_folder_druid_class_init (GbzFolderDruidClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_folder_druid_finalize;
	object_class->get_property = gbz_folder_druid_get_property;
	object_class->set_property = gbz_folder_druid_set_property;

	g_object_class_install_property (object_class,
					 PROP_TREE,
					 g_param_spec_pointer ("tree",
							       _("Tree"),
							       _("Current tree."),
							       G_PARAM_READWRITE));
}

static void
gbz_folder_druid_instance_init (GbzFolderDruid *folder_druid)
{
	GbzFolderDruidPrivate *priv;

	priv = g_new0 (GbzFolderDruidPrivate, 1);
	folder_druid->priv = priv;

	gtk_window_set_title (GTK_WINDOW (folder_druid), _("New Folder"));

	priv->druid = GNOME_DRUID (gnome_druid_new ());
	priv->folder_page = create_folder_page (folder_druid);

	gnome_druid_append_page (priv->druid, priv->folder_page);
	gnome_druid_set_show_help (priv->druid, TRUE);
	gnome_druid_set_show_finish (priv->druid, FALSE);

	g_signal_connect_after (G_OBJECT (priv->folder_page), "next",
				G_CALLBACK (folder_next_cb), folder_druid);

	gnome_druid_set_buttons_sensitive (priv->druid, FALSE, TRUE, TRUE, TRUE);
	gnome_druid_set_page (priv->druid, priv->folder_page);

	gtk_container_add (GTK_CONTAINER (folder_druid), GTK_WIDGET (priv->druid));
	gtk_widget_show_all (GTK_WIDGET (priv->druid));

	gtk_window_set_position (GTK_WINDOW (folder_druid),
				 GTK_WIN_POS_CENTER_ON_PARENT);

	g_signal_connect_object (priv->druid, "cancel",
				 G_CALLBACK (gtk_widget_destroy),
				 folder_druid,
				 G_CONNECT_SWAPPED);

	g_signal_connect_object (priv->druid, "destroy",
				 G_CALLBACK (gtk_widget_destroy),
				 folder_druid,
				 G_CONNECT_SWAPPED);
}

static void
gbz_folder_druid_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gbz_folder_druid_get_property (GObject    *object,
			       guint       prop_id,
			       GValue     *value,
			       GParamSpec *pspec)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (object);

	switch (prop_id) {
		case PROP_TREE:
			g_value_set_pointer (value, druid->priv->tree);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
gbz_folder_druid_set_property (GObject      *object,
			       guint         prop_id,
			       const GValue *value,
			       GParamSpec   *pspec)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (object);

	switch (prop_id) {
		case PROP_TREE:
			druid->priv->tree = g_value_get_pointer (value);
			druid->priv->folder_model = gbz_tree_get_model (druid->priv->tree);

			gtk_tree_view_set_model (GTK_TREE_VIEW (druid->priv->folder_tree),
						 druid->priv->folder_model);
			gtk_tree_view_expand_all (GTK_TREE_VIEW (druid->priv->folder_tree));
			break;
		default:
			break;
	}
}

static GnomeDruidPage *
create_folder_page (GbzFolderDruid *druid)
{
	GbzFolderDruidPrivate *priv = druid->priv;
	GtkWidget *page, *label, *tree_sw, *menu, *menuitem;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	int i, height;

	page = gnome_druid_page_standard_new_with_vals (_("New Folder"),
							NULL, NULL);

	label = gtk_label_new_with_mnemonic (_("Folder _type:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    label, FALSE, TRUE, 0);

	priv->type_option = gtk_option_menu_new ();
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    priv->type_option, FALSE, TRUE, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->type_option);

	menu = gtk_menu_new ();
	for (i = 0; type_list[i] != NULL; i++) {
		menuitem = gtk_menu_item_new_with_label (type_list[i]);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (priv->type_option), menu);
	gtk_widget_show (menu);

	label = gtk_label_new_with_mnemonic (_("Specify _where to create the folder:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    label, FALSE, TRUE, 0);

	priv->folder_tree = gtk_tree_view_new ();
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (priv->folder_tree), FALSE);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->folder_tree);

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Folders");

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 set_pixbuf,
						 NULL, NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 set_folder_text,
						 NULL, NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->folder_tree), column);

	tree_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (tree_sw),
					     GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (tree_sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (tree_sw), priv->folder_tree);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    tree_sw, TRUE, TRUE, 0);

	/* Set a decent height for the scrolled window. */
	gtk_cell_renderer_get_size (renderer, priv->folder_tree, NULL, NULL,
				    NULL, NULL, &height);
	gtk_widget_set_size_request (tree_sw, -1, height * 6);

	return GNOME_DRUID_PAGE (page);
}

static void
set_pixbuf (GtkTreeViewColumn *tree_column,
	    GtkCellRenderer   *cell,
	    GtkTreeModel      *model,
	    GtkTreeIter       *iter,
	    gpointer           data)
{
	GbzTreeData *tdata;
	GdkPixbuf *pixbuf;
	static GdkPixbuf *account_pixbuf = NULL;

	gtk_tree_model_get (model, iter, 0, &tdata, -1);

	switch (tdata->type) {
	case GBZ_TREE_NODE_ACCOUNT:
		if (!account_pixbuf) {
			GError *err = NULL;
			account_pixbuf = gdk_pixbuf_new_from_file (ICONSDIR "/bugzilla-buddy-database_16x16.png", &err);
			if (err) {
				g_print (_("Error: %s"), err->message);
				g_error_free (err);
				account_pixbuf = NULL;
			}
		}
		pixbuf = account_pixbuf;
		break;
	case GBZ_TREE_NODE_FOLDER:
	case GBZ_TREE_NODE_QUERY:
		pixbuf = NULL;
		break;
	default:
		pixbuf = NULL;
	}

	g_object_set (GTK_CELL_RENDERER (cell), "pixbuf", pixbuf, NULL);
	gbz_tree_data_free (tdata);
}

static void
set_folder_text (GtkTreeViewColumn *tree_column,
		 GtkCellRenderer   *cell,
		 GtkTreeModel      *model,
		 GtkTreeIter       *iter,
		 gpointer           data)
{
	GbzTreeData *tdata;
	GbzAccount *account;
	char *name;

	gtk_tree_model_get (model, iter, 0, &tdata, -1);

	switch (tdata->type) {
	case GBZ_TREE_NODE_ACCOUNT:
		account = GBZ_ACCOUNT (tdata->data);
		g_object_get (account, "bugzilla", &name, NULL);
		g_object_set (GTK_CELL_RENDERER (cell), "text", 
			      name, NULL);
		break;
	case GBZ_TREE_NODE_FOLDER:
	case GBZ_TREE_NODE_QUERY:
		g_object_set (GTK_CELL_RENDERER (cell), "text", 
			      tdata->data, NULL);
		break;
	default:
		g_object_set (GTK_CELL_RENDERER (cell), "text", NULL, NULL);
	}

	gbz_tree_data_free (tdata);
}

static GnomeDruidPage *
create_plain_page (GbzFolderDruid *druid)
{
	GbzFolderDruidPrivate *priv = druid->priv;
	GtkWidget *page, *label;

	page = gnome_druid_page_standard_new_with_vals (_("Plain Folder"),
							NULL, NULL);

	label = gtk_label_new_with_mnemonic (_("Folder _name:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    label, FALSE, TRUE, 0);

	priv->name_entry = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    priv->name_entry, FALSE, TRUE, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->name_entry);

	return GNOME_DRUID_PAGE (page);
}

static GnomeDruidPage *
create_product_page (GbzFolderDruid *druid)
{
	GbzFolderDruidPrivate *priv = druid->priv;
	GtkWidget *page, *label, *tree_sw;
	GtkCellRenderer *renderer;

	page = gnome_druid_page_standard_new_with_vals (_("Add Product"),
							NULL, NULL);

	label = gtk_label_new_with_mnemonic (_("Select the _product you wish to add:"));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    label, FALSE, FALSE, 0);

	/* Create product tree. */
	priv->product_model = GTK_TREE_MODEL (gtk_list_store_new (2, G_TYPE_STRING, 
								  G_TYPE_POINTER));
	priv->product_tree = gtk_tree_view_new_with_model (priv->product_model);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (priv->product_tree), FALSE);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->product_tree);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (priv->product_tree),
						     -1, _("Product"), renderer,
						     "text", 0, NULL);

	tree_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (tree_sw),
					     GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (tree_sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (tree_sw), priv->product_tree);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    tree_sw, TRUE, TRUE, 0);

	priv->refresh_hbox = gtk_hbox_new (FALSE, 5);
	priv->refresh_button = gtk_button_new_with_mnemonic (_("_Refresh Product List"));
	gtk_box_pack_end (GTK_BOX (priv->refresh_hbox),
			  priv->refresh_button, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    priv->refresh_hbox, FALSE, FALSE, 0);

	return GNOME_DRUID_PAGE (page);
}

static GnomeDruidPage *
create_component_page (GbzFolderDruid *druid)
{
	GbzFolderDruidPrivate *priv = druid->priv;
	GtkWidget *page, *label, *tree_sw;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;

	page = gnome_druid_page_standard_new_with_vals (_("Select Components"),
							NULL, NULL);

	label = gtk_label_new_with_mnemonic (_("Select the _components you wish to add:"));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    label, FALSE, FALSE, 0);

	/* Create product tree. */
	priv->component_model = GTK_TREE_MODEL (gtk_tree_store_new (3, G_TYPE_STRING,
								    G_TYPE_BOOLEAN,
								    G_TYPE_INT));
	priv->component_tree = gtk_tree_view_new_with_model (priv->component_model);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (priv->component_tree), FALSE);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->component_tree);

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Components");

	renderer = gtk_cell_renderer_toggle_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 set_toggle,
						 druid, NULL);
	g_signal_connect (G_OBJECT (renderer), "toggled",
			  G_CALLBACK (cell_toggled_cb), druid);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 set_component_text,
						 druid, NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->component_tree), column);

	tree_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (tree_sw),
					     GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (tree_sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (tree_sw), priv->component_tree);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    tree_sw, TRUE, TRUE, 0);

	return GNOME_DRUID_PAGE (page);
}

static void
set_toggle (GtkTreeViewColumn *tree_column,
	    GtkCellRenderer   *cell,
	    GtkTreeModel      *model,
	    GtkTreeIter       *iter,
	    gpointer           data)
{
	gboolean toggled, toggled2 = FALSE;
	GtkTreeIter parent;

	if (gtk_tree_model_iter_parent (model, &parent, iter))
		gtk_tree_model_get (model, &parent, 1, &toggled2, -1);

	gtk_tree_model_get (model, iter, 1, &toggled, -1);
	g_object_set (GTK_CELL_RENDERER (cell), "active", toggled,
		      "activatable", !toggled2, NULL);
}

static void
set_component_text (GtkTreeViewColumn *tree_column,
		    GtkCellRenderer   *cell,
		    GtkTreeModel      *model,
		    GtkTreeIter       *iter,
		    gpointer           data)
{
	char *name;

	gtk_tree_model_get (model, iter, 0, &name, -1);
	g_object_set (GTK_CELL_RENDERER (cell), "text", name, NULL);
}

static void
cell_toggled_cb (GtkCellRendererToggle *renderer,
		 const char            *path,
		 gpointer               data)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (data);
	GtkTreeIter iter;
	GtkTreePath *tree_path;
	gboolean toggled;

	tree_path = gtk_tree_path_new_from_string (path);
	gtk_tree_model_get_iter (druid->priv->component_model, &iter, tree_path);

	gtk_tree_model_get (druid->priv->component_model, &iter, 1, &toggled, -1);
	gtk_tree_store_set (GTK_TREE_STORE (druid->priv->component_model),
			    &iter, 1, !toggled, -1);

	gtk_cell_renderer_toggle_set_active (renderer, !toggled);

	gtk_tree_path_free (tree_path);
}

static GnomeDruidPage *
create_query_page (GbzFolderDruid *druid)
{
	GbzFolderDruidPrivate *priv = druid->priv;
	GtkWidget *page;

	page = gnome_druid_page_standard_new_with_vals (_("Create Query"),
							NULL, NULL);

	priv->query_editor = gbz_query_editor_new ();
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    GTK_WIDGET (priv->query_editor), TRUE, TRUE, 0);

	return GNOME_DRUID_PAGE (page);
}

static void
find_account (GbzFolderDruid *druid,
	      GtkTreePath    *path)
{
	GtkTreeIter iter;
	GbzTreeData *data;

	gtk_tree_model_get_iter (druid->priv->folder_model, &iter, path);
	gtk_tree_model_get (druid->priv->folder_model, &iter, 0, &data, -1);

	if (data->type == GBZ_TREE_NODE_ACCOUNT) {
		druid->priv->selected_account = GBZ_ACCOUNT (data->data);
	} else {
		gtk_tree_path_up (path);
		find_account (druid, path);
	}
}

static gboolean
folder_next_cb (GnomeDruidPage *page,
		GtkWidget      *widget,
		gpointer        data)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (data);
	GbzFolderDruidPrivate *priv = druid->priv;
	GtkTreeSelection *sel;
	GtkTreeIter iter;
	GtkWidget *dialog;
	GtkTreePath *path;
	int selected;

	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->folder_tree));

	if (!gtk_tree_selection_get_selected (sel, NULL, &iter)) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (druid),
						 GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_INFO,
						 GTK_BUTTONS_CLOSE,
						 _("You need to select a folder."));
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		return TRUE;
	}

	/* Find account to which to the selected folder belongs. */
	path = gtk_tree_model_get_path (priv->folder_model, &iter);
	priv->selected_path = gtk_tree_path_copy (path);
	find_account (druid, path);
	gtk_tree_path_free (path);

	selected = gtk_option_menu_get_history (GTK_OPTION_MENU (priv->type_option));
	switch (selected) {
		case 0: /* Plain. */
			priv->plain_page = create_plain_page (druid);
			gnome_druid_append_page (priv->druid, priv->plain_page);
			gtk_widget_show_all (GTK_WIDGET (priv->plain_page));
			g_signal_connect_after (G_OBJECT (priv->plain_page),
						"prepare", G_CALLBACK (plain_prepare_cb),
						druid);
			g_signal_connect (G_OBJECT (priv->plain_page),
					  "finish", G_CALLBACK (plain_finish_cb),
					  druid);
			break;
		case 1: /* Product. */
			priv->product_page = create_product_page (druid);
			priv->component_page = create_component_page (druid);
			gnome_druid_append_page (priv->druid, priv->product_page);
			gnome_druid_append_page (priv->druid, priv->component_page);
			gtk_widget_show_all (GTK_WIDGET (priv->product_page));
			gtk_widget_show_all (GTK_WIDGET (priv->component_page));
			g_signal_connect_after (G_OBJECT (priv->product_page),
						"prepare", G_CALLBACK (product_prepare_cb),
						druid);
			g_signal_connect (G_OBJECT (priv->product_page),
					  "next", G_CALLBACK (product_next_cb),
					  druid);
			g_signal_connect_after (G_OBJECT (priv->component_page),
						"prepare", G_CALLBACK (component_prepare_cb),
						druid);
			g_signal_connect (G_OBJECT (priv->component_page),
					  "finish", G_CALLBACK (component_finish_cb),
					  druid);
			break;
		case 2: /* Query. */
			priv->query_page = create_query_page (druid);
			gnome_druid_append_page (priv->druid, priv->query_page);
			gtk_widget_show_all (GTK_WIDGET (priv->query_page));
			g_signal_connect_after (G_OBJECT (priv->query_page),
						"prepare", G_CALLBACK (product_prepare_cb),
						druid);
			break;
	}

	return FALSE;
}

static gboolean
plain_prepare_cb (GnomeDruidPage *page,
		  GtkWidget      *widget,
		  gpointer        data)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (data);

	gnome_druid_set_show_finish (GNOME_DRUID (druid->priv->druid), TRUE);
	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid->priv->druid),
					   TRUE, FALSE, TRUE, TRUE);

	return TRUE;
}

static gboolean
plain_finish_cb (GnomeDruidPage *page,
		 GtkWidget      *widget,
		 gpointer        data)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (data);
	const char *name;
	GtkWidget *dialog;
	GtkTreeIter iter, parent_iter;
	GbzTreeData *tdata;

	name = gtk_entry_get_text (GTK_ENTRY (druid->priv->name_entry));
	if (!name || strlen (name) == 0) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (druid),
						 GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_INFO,
						 GTK_BUTTONS_CLOSE,
						 _("You need to enter a folder name."));
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		return TRUE;
	}

	tdata = gbz_tree_data_new (GBZ_TREE_NODE_FOLDER, g_strdup (name));

	gtk_tree_model_get_iter (druid->priv->folder_model, &parent_iter,
				 druid->priv->selected_path);
	gtk_tree_store_append (GTK_TREE_STORE (druid->priv->folder_model),
			       &iter, &parent_iter);
	gtk_tree_store_set (GTK_TREE_STORE (druid->priv->folder_model), &iter,
			    0, tdata, -1);

	gtk_widget_destroy (GTK_WIDGET (druid));

	return FALSE;
}

static gboolean
product_prepare_cb (GnomeDruidPage *page,
		    GtkWidget      *widget,
		    gpointer        data)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (data);
	char *bugzilla;
	GSList *products, *l;
	GtkTreeIter iter;

	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid->priv->druid),
					   TRUE, TRUE, TRUE, TRUE);

	gtk_list_store_clear (GTK_LIST_STORE (druid->priv->product_model));

	g_object_get (G_OBJECT (druid->priv->selected_account), "bugzilla",
		      &bugzilla, NULL);
	products = gbz_tree_get_product_list (druid->priv->tree, bugzilla);
	for (l = products; l; l = l->next) {
		GbzProductInfo *info = l->data;
		gtk_list_store_append (GTK_LIST_STORE (druid->priv->product_model), &iter);
		gtk_list_store_set (GTK_LIST_STORE (druid->priv->product_model),
				    &iter, 0, info->name, 1, info, -1);
	}

	return TRUE;
}

static gboolean
product_next_cb (GnomeDruidPage *page,
		 GtkWidget      *widget,
		 gpointer        data)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (data);
	GtkTreeSelection *sel;
	GtkTreeIter iter;
	GtkWidget *dialog;
	GbzProductInfo *info;

	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (druid->priv->product_tree));

	if (!gtk_tree_selection_get_selected (sel, NULL, &iter)) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (druid),
						 GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_INFO,
						 GTK_BUTTONS_CLOSE,
						 _("You need to select a product."));
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		return TRUE;
	}

	gtk_tree_model_get (druid->priv->product_model, &iter, 1, &info, -1);
	druid->priv->selected_product = info;

	return FALSE;
}

static gboolean
component_prepare_cb (GnomeDruidPage *page,
		      GtkWidget      *widget,
		      gpointer        data)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (data);
	GbzProductInfo *info = druid->priv->selected_product;
	GSList *l;
	GtkTreeIter iter, iter2;

	gnome_druid_set_show_finish (GNOME_DRUID (druid->priv->druid), TRUE);
	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid->priv->druid),
					   TRUE, FALSE, TRUE, TRUE);

	gtk_tree_store_clear (GTK_TREE_STORE (druid->priv->component_model));

	/* Add components. */
	gtk_tree_store_append (GTK_TREE_STORE (druid->priv->component_model), &iter, NULL);
	gtk_tree_store_set (GTK_TREE_STORE (druid->priv->component_model),
			    &iter, 0, _("components"), 1, FALSE, 2,
			    TYPE_COMPONENT, -1);
	for (l = info->components; l; l = l->next) {
		char *component = l->data;
		gtk_tree_store_append (GTK_TREE_STORE (druid->priv->component_model),
				       &iter2, &iter);
		gtk_tree_store_set (GTK_TREE_STORE (druid->priv->component_model),
				    &iter2, 0, component, 1, FALSE, 2,
				    TYPE_COMPONENT, -1);
	}

	/* Add versions. */
	gtk_tree_store_append (GTK_TREE_STORE (druid->priv->component_model), &iter, NULL);
	gtk_tree_store_set (GTK_TREE_STORE (druid->priv->component_model),
			    &iter, 0, _("versions"), 1, FALSE, 2, TYPE_VERSION, -1);
	for (l = info->versions; l; l = l->next) {
		char *version = l->data;
		gtk_tree_store_append (GTK_TREE_STORE (druid->priv->component_model),
				       &iter2, &iter);
		gtk_tree_store_set (GTK_TREE_STORE (druid->priv->component_model),
				    &iter2, 0, version, 1, FALSE, 2, TYPE_VERSION, -1);
	}

	/* Add milestones. */
	gtk_tree_store_append (GTK_TREE_STORE (druid->priv->component_model), &iter, NULL);
	gtk_tree_store_set (GTK_TREE_STORE (druid->priv->component_model),
			    &iter, 0, _("milestones"), 1, FALSE, 2,
			    TYPE_MILESTONE, -1);
	for (l = info->milestones; l; l = l->next) {
		char *milestone = l->data;
		gtk_tree_store_append (GTK_TREE_STORE (druid->priv->component_model),
				       &iter2, &iter);
		gtk_tree_store_set (GTK_TREE_STORE (druid->priv->component_model),
				    &iter2, 0, milestone, 1, FALSE, 2,
				    TYPE_MILESTONE, -1);
	}

	return TRUE;
}

static gboolean
component_finish_cb (GnomeDruidPage *page,
		     GtkWidget      *widget,
		     gpointer        data)
{
	GbzFolderDruid *druid = GBZ_FOLDER_DRUID (data);
	GbzFolderDruidPrivate *priv = druid->priv;
	GbzTreeData *tdata;
	GtkTreeIter parent, node, child, child2, root;
	char *name;
	gboolean parent_toggled, toggled, folder_created;

	/* Add product folder. */
	tdata = gbz_tree_data_new (GBZ_TREE_NODE_FOLDER,
				   g_strdup (priv->selected_product->name));

	gtk_tree_model_get_iter (priv->folder_model, &parent, priv->selected_path);
	gtk_tree_store_append (GTK_TREE_STORE (priv->folder_model), &root, &parent);
	gtk_tree_store_set (GTK_TREE_STORE (priv->folder_model), &root, 0, tdata, -1);

	/* Add selected components. */
	gtk_tree_model_get_iter_root (priv->component_model, &parent);
	gtk_tree_model_get (priv->component_model, &parent, 1, &parent_toggled, -1);

	/* Add components. */
	folder_created = FALSE;
	gtk_tree_model_iter_children (priv->component_model, &node, &parent);
	do {
		gtk_tree_model_get (priv->component_model, &node, 0, &name,
				    1, &toggled, -1);

		if (parent_toggled || toggled) {
			if (!folder_created) {
				/* Add component folder. */
				tdata = gbz_tree_data_new (GBZ_TREE_NODE_FOLDER,
							   g_strdup (_("components")));
				gtk_tree_store_append (GTK_TREE_STORE (priv->folder_model),
						       &child, &root);
				gtk_tree_store_set (GTK_TREE_STORE (priv->folder_model),
						    &child, 0, tdata, -1);
				folder_created = TRUE;
			}

			/* Add component. */
			tdata = gbz_tree_data_new (GBZ_TREE_NODE_QUERY, g_strdup (name));
			gtk_tree_store_append (GTK_TREE_STORE (priv->folder_model),
					       &child2, &child);
			gtk_tree_store_set (GTK_TREE_STORE (priv->folder_model),
					    &child2, 0, tdata, -1);
		}
	} while (gtk_tree_model_iter_next (priv->component_model, &node));

	/* Add versions. */
	folder_created = FALSE;
	gtk_tree_model_iter_next (priv->component_model, &parent);
	gtk_tree_model_get (priv->component_model, &parent, 1, &parent_toggled, -1);
	gtk_tree_model_iter_children (priv->component_model, &node, &parent);
	do {
		gtk_tree_model_get (priv->component_model, &node, 0, &name,
				    1, &toggled, -1);

		if (parent_toggled || toggled) {
			if (!folder_created) {
				/* Add component folder. */
				tdata = gbz_tree_data_new (GBZ_TREE_NODE_FOLDER,
							   g_strdup (_("versions")));
				gtk_tree_store_append (GTK_TREE_STORE (priv->folder_model),
						       &child, &root);
				gtk_tree_store_set (GTK_TREE_STORE (priv->folder_model),
						    &child, 0, tdata, -1);
				folder_created = TRUE;
			}

			/* Add component. */
			tdata = gbz_tree_data_new (GBZ_TREE_NODE_QUERY, g_strdup (name));
			gtk_tree_store_append (GTK_TREE_STORE (priv->folder_model),
					       &child2, &child);
			gtk_tree_store_set (GTK_TREE_STORE (priv->folder_model),
					    &child2, 0, tdata, -1);
		}
	} while (gtk_tree_model_iter_next (priv->component_model, &node));

	/* Add versions. */
	folder_created = FALSE;
	gtk_tree_model_iter_next (priv->component_model, &parent);
	gtk_tree_model_get (priv->component_model, &parent, 1, &parent_toggled, -1);
	gtk_tree_model_iter_children (priv->component_model, &node, &parent);
	do {
		gtk_tree_model_get (priv->component_model, &node, 0, &name,
				    1, &toggled, -1);

		if (parent_toggled || toggled) {
			if (!folder_created) {
				/* Add component folder. */
				tdata = gbz_tree_data_new (GBZ_TREE_NODE_FOLDER,
							   g_strdup (_("milestones")));
				gtk_tree_store_append (GTK_TREE_STORE (priv->folder_model),
						       &child, &root);
				gtk_tree_store_set (GTK_TREE_STORE (priv->folder_model),
						    &child, 0, tdata, -1);
				folder_created = TRUE;
			}

			/* Add component. */
			tdata = gbz_tree_data_new (GBZ_TREE_NODE_QUERY, g_strdup (name));
			gtk_tree_store_append (GTK_TREE_STORE (priv->folder_model),
					       &child2, &child);
			gtk_tree_store_set (GTK_TREE_STORE (priv->folder_model),
					    &child2, 0, tdata, -1);
		}
	} while (gtk_tree_model_iter_next (priv->component_model, &node));

	gtk_widget_destroy (GTK_WIDGET (druid));

	return FALSE;
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GtkWidget *
gbz_folder_druid_new (GbzTree *tree)
{
	return GTK_WIDGET (g_object_new (GBZ_TYPE_FOLDER_DRUID, "tree", tree, NULL));
}
