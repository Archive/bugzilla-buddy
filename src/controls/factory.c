/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <bonobo.h>
#include <bonobo/bonobo-shlib-factory.h>
#include <bonobo-activation/bonobo-activation.h>
#include <string.h>
#include "tree-object.h"
#include "commands.h"
#include "stock.h"

static BonoboUIVerb tree_folder_verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("NewAccount", new_account),
	BONOBO_UI_UNSAFE_VERB ("NewFolder", new_folder),
	BONOBO_UI_UNSAFE_VERB ("UpdateProducts", update_products),
	BONOBO_UI_VERB_END
};

static void
folder_tree_set_frame_cb (BonoboControl *control,
			  gpointer       data)
{
	GbzTree *tree = GBZ_TREE (data);
	Bonobo_ControlFrame frame;
	BonoboUIComponent *ui_component;
	Bonobo_UIContainer ui_container;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	frame = bonobo_control_get_control_frame (control, &ev);
	if (!BONOBO_EX (&ev) && frame != CORBA_OBJECT_NIL) {
		CORBA_Object_release (frame, NULL);

		ui_component = bonobo_control_get_ui_component (control);

		g_object_set_data (G_OBJECT (tree), "ui_component", ui_component);

		ui_container= bonobo_control_get_remote_ui_container (control, &ev);
		if (BONOBO_EX (&ev) || ui_container == CORBA_OBJECT_NIL)
			return;

		bonobo_ui_component_set_container (ui_component,
						   ui_container,
						   NULL);

		bonobo_ui_component_freeze (ui_component, NULL);

		bonobo_object_release_unref (ui_container, NULL);

		bonobo_ui_component_add_verb_list_with_data (ui_component,
							     tree_folder_verbs,
							     control);

		bonobo_ui_util_set_ui (ui_component, DATADIR, "gbz-tree.xml",
				       "FolderTree", NULL);

		bonobo_ui_component_thaw (ui_component, NULL);
	}

	CORBA_exception_free (&ev);
}

static BonoboObject *
folder_tree_factory (BonoboGenericFactory *fact,
		     void                 *closure)
{
	BonoboControl *control;
	GtkWidget *tree;
	GbzTreeObject *treeobj;
	BonoboPropertyBag *pb;

	/* Create the control. */
	tree = gbz_tree_new ();
	gtk_widget_show_all (tree);
	treeobj = gbz_tree_object_new (GBZ_TREE (tree));
	control = bonobo_control_new (tree);

	/* Add GNOME_Bugzilla_FolderTree interface to control. */
	bonobo_object_add_interface (BONOBO_OBJECT (control), 
				     BONOBO_OBJECT (treeobj));

	g_object_set_data (G_OBJECT (control), "GbzTree", tree);
	g_object_set_data (G_OBJECT (control), "GbzTreeObject", treeobj);

	/* UI initialization takes place when the control frame is set. */
	g_signal_connect (G_OBJECT (control),
			  "set_frame",
			  G_CALLBACK (folder_tree_set_frame_cb),
			  tree);

	return BONOBO_OBJECT (control);
}

static BonoboObject *
control_factory (BonoboGenericFactory *factory,
		 const char           *component_id,
		 gpointer              closure)
{
	static gboolean initialized = FALSE;

	if (!initialized) {
		stock_init ();
		initialized = TRUE;
	}

	if (!strcmp (component_id, "OAFIID:GNOME_Bugzilla_FolderTree"))
		return folder_tree_factory (factory, closure);

	return NULL;
}

BONOBO_ACTIVATION_SHLIB_FACTORY ("OAFIID:GNOME_Bugzilla_ControlFactory",
				 "Factory for the Gnome Bugzilla controls.",
				 control_factory, NULL);
