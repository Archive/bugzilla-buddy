/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gtk/gtkiconfactory.h>
#include <gtk/gtkstock.h>
#include <bonobo/bonobo-i18n.h>
#include "stock.h"

static GtkStockItem builtin_icons [] = {
	{ GNOME_BUGZILLA_STOCK_CONNECT, N_("Connect"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_BUGZILLA_STOCK_DISCONNECT, N_("Disconnect"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_BUGZILLA_STOCK_DATABASE, N_("Database"), 0, 0, GETTEXT_PACKAGE }
};

static void
add_sized (GtkIconFactory *factory,
	   const char     *pixfile,
	   GtkIconSize     size,
	   const char     *stock_id)
{
	GtkIconSet *set;
	GtkIconSource *source;
	GdkPixbuf *pixbuf;
	GError *err = NULL;

	pixbuf = gdk_pixbuf_new_from_file (pixfile, &err);
	if (err) {
		g_print (_("Error: %s"), err->message);
		g_error_free (err);
		return;
	}

	source = gtk_icon_source_new ();
	gtk_icon_source_set_pixbuf (source, pixbuf);
	gtk_icon_source_set_size (source, size);

	set = gtk_icon_set_new ();
	gtk_icon_set_add_source (set, source);

	gtk_icon_factory_add (factory, stock_id, set);

	g_object_unref (G_OBJECT (pixbuf));
	gtk_icon_source_free (source);
	gtk_icon_set_unref (set);
}

static void
get_stock_icons (GtkIconFactory *factory)
{
	add_sized (factory, ICONSDIR "/bugzilla-buddy-connect_24x24.png",
		   GTK_ICON_SIZE_BUTTON, GNOME_BUGZILLA_STOCK_CONNECT);
	add_sized (factory, ICONSDIR "/bugzilla-buddy-database_24x24.png",
		   GTK_ICON_SIZE_BUTTON, GNOME_BUGZILLA_STOCK_DATABASE);
	add_sized (factory, ICONSDIR "/bugzilla-buddy-disconnect_24x24.png",
		   GTK_ICON_SIZE_BUTTON, GNOME_BUGZILLA_STOCK_DISCONNECT);
}

void
stock_init (void)
{
	GtkIconFactory *factory = gtk_icon_factory_new ();

	get_stock_icons (factory);
	gtk_icon_factory_add_default (factory);

	gtk_stock_add_static (builtin_icons, G_N_ELEMENTS (builtin_icons));
}
