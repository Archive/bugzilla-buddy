/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_TREE_H
#define GBZ_TREE_H

#include <gtk/gtk.h>
#include "account.h"

G_BEGIN_DECLS

#define GBZ_TYPE_TREE			(gbz_tree_get_type ())
#define GBZ_TREE(obj)			(GTK_CHECK_CAST ((obj), GBZ_TYPE_TREE, GbzTree))
#define GBZ_TREE_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GBZ_TYPE_TREE, GbzTreeClass))
#define GBZ_IS_TREE(obj)		(GTK_CHECK_TYPE ((obj), GBZ_TYPE_TREE))
#define GBZ_IS_TREE_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GBZ_TYPE_TREE))

typedef struct _GbzTree			GbzTree;
typedef struct _GbzTreePrivate		GbzTreePrivate;
typedef struct _GbzTreeClass		GbzTreeClass;
typedef struct _GbzProductInfo		GbzProductInfo;

struct _GbzTree {
	GtkVBox parent;

	GbzTreePrivate *priv;
};

struct _GbzTreeClass {
	GtkVBoxClass parent_class;
};

struct _GbzProductInfo {
	char *name;
	GSList *components;
	GSList *versions;
	GSList *milestones;
};

/* Creation. */
GType                     gbz_tree_get_type (void);
GtkWidget                *gbz_tree_new (void);

GSList                   *gbz_tree_get_product_list (GbzTree    *tree,
						     const char *bugzilla);
void                      gbz_tree_set_product_list (GbzTree    *tree,
						     const char *bugzilla,
						     GSList     *products);

/* Add/remove account. */
void                      gbz_tree_add_account    (GbzTree    *tree,
						   GbzAccount *account);
void                      gbz_tree_remove_account (GbzTree    *tree,
						   GbzAccount *account);
GSList                   *gbz_tree_get_accounts   (GbzTree    *tree);

/* Get/set active account. */
GbzAccount               *gbz_tree_get_active_account (GbzTree    *tree);
void                      gbz_tree_set_active_account (GbzTree    *tree,
						       GbzAccount *account);

/* Load/save account info. */
void                      gbz_tree_load_config (GbzTree *tree);
void                      gbz_tree_save_config (GbzTree *tree);

GtkTreeModel             *gbz_tree_get_model (GbzTree *tree);

G_END_DECLS

#endif /* GBZ_TREE_H */
