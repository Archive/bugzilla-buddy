/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <libbonobo.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <libgnome/gnome-util.h>
#include <libxml/tree.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "tree.h"
#include "tree-data.h"

struct _GbzTreePrivate {
	GtkWidget *tree_sw;
	GtkWidget *tree;

	GtkTreeModel *tree_model;

	GSList *accounts;
	GHashTable *product_lists;

	xmlDocPtr doc;
};

/* Prototypes. */
static void gbz_tree_finalize (GObject *object);

static void set_pixbuf (GtkTreeViewColumn *tree_column,
			GtkCellRenderer   *cell,
			GtkTreeModel      *model,
			GtkTreeIter       *iter,
			gpointer           data);

static void set_text (GtkTreeViewColumn *tree_column,
		      GtkCellRenderer   *cell,
		      GtkTreeModel      *model,
		      GtkTreeIter       *iter,
		      gpointer           data);

/* Boilerplate. */
GNOME_CLASS_BOILERPLATE (GbzTree, gbz_tree,
			 GtkVBox, GTK_TYPE_VBOX);

/* Private methods. */
static void
gbz_tree_class_init (GbzTreeClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_tree_finalize;
}

static void
gbz_tree_instance_init (GbzTree *tree)
{
	GbzTreePrivate *priv;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	priv = g_new0 (GbzTreePrivate, 1);
	tree->priv = priv;
	priv->accounts = NULL;
	priv->product_lists = g_hash_table_new (g_str_hash, g_str_equal);

	/* Create tree model. */
	priv->tree_model = GTK_TREE_MODEL (gtk_tree_store_new (1, GBF_TYPE_TREE_DATA));

	/* Create scrolled window. */
	priv->tree_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (priv->tree_sw),
					     GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->tree_sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	/* Create tree view. */
	priv->tree = gtk_tree_view_new_with_model (priv->tree_model);
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (priv->tree), TRUE);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (priv->tree), FALSE);

	gtk_container_add (GTK_CONTAINER (priv->tree_sw), priv->tree);

	/*g_signal_connect (G_OBJECT (priv->tree), "row_activated",
			  G_CALLBACK (row_activated_cb), tree);*/

	/* Create ID column. */
	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Folders");

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 set_pixbuf,
						 tree, NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 set_text,
						 tree, NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->tree), column);

	gtk_box_pack_start (GTK_BOX (tree), priv->tree_sw, TRUE, TRUE, 0);
	gtk_widget_show_all (priv->tree_sw);
}

static void
gbz_tree_finalize (GObject *object)
{
	GbzTree *tree = GBZ_TREE (object);

	gbz_tree_save_config (tree);

	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
set_pixbuf (GtkTreeViewColumn *tree_column,
	    GtkCellRenderer   *cell,
	    GtkTreeModel      *model,
	    GtkTreeIter       *iter,
	    gpointer           data)
{
	GbzTree *tree = GBZ_TREE (data);
	GbzTreeData *tdata;
	GdkPixbuf *pixbuf;
	GtkTreePath *path;
	static GdkPixbuf *account_pixbuf = NULL;
	static GdkPixbuf *folder_open_pixbuf = NULL;
	static GdkPixbuf *folder_closed_pixbuf = NULL;

	gtk_tree_model_get (model, iter, 0, &tdata, -1);

	switch (tdata->type) {
	case GBZ_TREE_NODE_ACCOUNT:
		if (!account_pixbuf) {
			GError *err = NULL;
			account_pixbuf = gdk_pixbuf_new_from_file (ICONSDIR "/bugzilla-buddy-database_16x16.png", &err);
			if (err) {
				g_print (_("Error: %s"), err->message);
				g_error_free (err);
				account_pixbuf = NULL;
			}
		}
		pixbuf = account_pixbuf;
		break;
	case GBZ_TREE_NODE_FOLDER:
		path = gtk_tree_model_get_path (model, iter);
		if (gtk_tree_view_row_expanded (GTK_TREE_VIEW (tree->priv->tree),
					       path)) {
			if (!folder_open_pixbuf) {
				GError *err = NULL;
				folder_open_pixbuf = gdk_pixbuf_new_from_file (ICONSDIR "/folder-open.png", &err);
				if (err) {
					g_print (_("Error: %s"), err->message);
					g_error_free (err);
					folder_open_pixbuf = NULL;
				}
			}
			pixbuf = folder_open_pixbuf;
		} else {
			if (!folder_closed_pixbuf) {
				GError *err = NULL;
				folder_closed_pixbuf = gdk_pixbuf_new_from_file (ICONSDIR "/folder-closed.png", &err);
				if (err) {
					g_print (_("Error: %s"), err->message);
					g_error_free (err);
					folder_closed_pixbuf = NULL;
				}
			}
			pixbuf = folder_closed_pixbuf;
		}
		break;
	case GBZ_TREE_NODE_QUERY:
		pixbuf = NULL;
		break;
	default:
		pixbuf = NULL;
	}

	g_object_set (GTK_CELL_RENDERER (cell), "pixbuf", pixbuf, NULL);
	gbz_tree_data_free (tdata);
}

static void
set_text (GtkTreeViewColumn *tree_column,
	  GtkCellRenderer   *cell,
	  GtkTreeModel      *model,
	  GtkTreeIter       *iter,
	  gpointer           data)
{
	GbzTreeData *tdata;
	GbzAccount *account;
	char *name;

	gtk_tree_model_get (model, iter, 0, &tdata, -1);

	switch (tdata->type) {
	case GBZ_TREE_NODE_ACCOUNT:
		account = GBZ_ACCOUNT (tdata->data);
		g_object_get (account, "bugzilla", &name, NULL);
		g_object_set (GTK_CELL_RENDERER (cell), "text", 
			      name, NULL);
		break;
	case GBZ_TREE_NODE_QUERY:
	case GBZ_TREE_NODE_FOLDER:
		g_object_set (GTK_CELL_RENDERER (cell), "text", 
			      tdata->data, NULL);
		break;
	default:
		g_object_set (GTK_CELL_RENDERER (cell), "text", NULL, NULL);
	}

	gbz_tree_data_free (tdata);
}

static void
load_data (xmlNodePtr node,
	   GSList   **list)
{
	xmlNodePtr child;

	for (child = node->children; child; child = child->next) {
		if (child->type == XML_ELEMENT_NODE) {
			*list = g_slist_append (*list, g_strdup (xmlNodeGetContent (child)));
		}
	}
}

static void
load_product_info (GbzTree    *tree,
		   const char *bugzilla)
{
	char *dir, *filename;
	xmlDocPtr doc;
	xmlNodePtr node, child;
	GSList *products = NULL;

	/* Check if the product list for this bugzilla server exists. */
	if (g_hash_table_lookup (tree->priv->product_lists, bugzilla))
		return;

	/* Check if products.xml file exists. */
	filename = g_strconcat (".bugzilla-buddy/", bugzilla, NULL);
	dir = gnome_util_prepend_user_home (filename);
	g_free (filename);
	if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		g_print ("Bugzilla folder %s does not exist\n", bugzilla);
		return;
	}

	/* Load products.xml file. */
	filename = g_strconcat (dir, "/products.xml");
	doc = xmlParseFile (filename);
	if (!doc) {
		g_print ("Error loading %s\n", filename);
		return;
	}
	g_free (filename);

	/* Parse product list. */
	for (node = doc->children->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			GbzProductInfo *info = g_new0 (GbzProductInfo, 1);
			info->name = g_strdup (xmlGetProp (node, "name"));
			info->components = NULL;
			info->versions = NULL;
			info->milestones = NULL;

			for (child = node->children; child; child = child->next) {
				if (!strcmp ("components", child->name))
					load_data (child, &info->components);
				else if (!strcmp ("versions", child->name))
					load_data (child, &info->versions);
				else if (!strcmp ("milestones", child->name))
					load_data (child, &info->milestones);
			}
			products = g_slist_append (products, info);
		}
	}

	g_hash_table_insert (tree->priv->product_lists, g_strdup (bugzilla), products);
}

static void
save_product_info (gpointer key,
		   gpointer value,
		   gpointer data)
{
	char *bugzilla, *temp, *filename;
	xmlDocPtr doc;
	GSList *products, *l, *j;
	xmlNodePtr node, child, child2;

	bugzilla = key;
	products = value;

	/* Create directory for bugzilla server. */
	temp = g_strconcat (".bugzilla-buddy/", bugzilla, NULL);
	filename = gnome_util_prepend_user_home (temp);
	g_free (temp);
	if (!g_file_test (filename, G_FILE_TEST_IS_DIR)) {
		mkdir (filename, 0755);
		if (!g_file_test (filename, G_FILE_TEST_IS_DIR)) {
			g_print ("Could not create .bugzilla-buddy/%s directory.\n", bugzilla);
			g_free (filename);
			return;
		}
	}

	/* Save product info in products.xml. */
	temp = g_strconcat (filename, "/products.xml", NULL);
	g_free (filename);

	doc = xmlNewDoc ("1.0");
	doc->children = xmlNewDocNode (doc, NULL, "product-list", NULL);

	for (l = products; l; l = l->next) {
		GbzProductInfo *info = l->data;
		node = xmlNewChild (doc->children, NULL, "product", NULL);
		xmlSetProp (node, "name", info->name);

		child = xmlNewChild (node, NULL, "components", NULL);
		for (j = info->components; j; j = j->next)
			child2 = xmlNewChild (child, NULL, "component", j->data);

		child = xmlNewChild (node, NULL, "versions", NULL);
		for (j = info->versions; j; j = j->next)
			child2 = xmlNewChild (child, NULL, "version", j->data);

		child = xmlNewChild (node, NULL, "milestones", NULL);
		for (j = info->milestones; j; j = j->next)
			child2 = xmlNewChild (child, NULL, "milestone", j->data);
	}

	xmlSaveFormatFile (temp, doc, TRUE);
	g_free (temp);
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GtkWidget *
gbz_tree_new (void)
{	GbzTree *tree;

	tree = GBZ_TREE (g_object_new (GBZ_TYPE_TREE, NULL));

	return GTK_WIDGET (tree);
}

void
gbz_tree_add_account (GbzTree    *tree,
		      GbzAccount *account)
{
	GtkTreeIter iter;
	GbzTreeData *data;

	g_return_if_fail (tree != NULL);
	g_return_if_fail (GBZ_IS_TREE (tree));
	g_return_if_fail (account != NULL);
	g_return_if_fail (GBZ_IS_ACCOUNT (account));

	tree->priv->accounts = g_slist_append (tree->priv->accounts, account);

	data = gbz_tree_data_new (GBZ_TREE_NODE_ACCOUNT, account);

	gtk_tree_store_append (GTK_TREE_STORE (tree->priv->tree_model), &iter, NULL);
	gtk_tree_store_set (GTK_TREE_STORE (tree->priv->tree_model),
			    &iter, 0, data, -1);

	g_object_set (G_OBJECT (account), "tree-model", tree->priv->tree_model,
		      "tree-iter", gtk_tree_iter_copy (&iter), NULL);
}

void
gbz_tree_remove_account (GbzTree    *tree,
			 GbzAccount *account)
{
	g_return_if_fail (tree != NULL);
	g_return_if_fail (GBZ_IS_TREE (tree));
	g_return_if_fail (account != NULL);
	g_return_if_fail (GBZ_IS_ACCOUNT (account));
}

GbzAccount *
gbz_tree_get_active_account (GbzTree *tree)
{
	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (GBZ_IS_TREE (tree), NULL);

	if (g_slist_length (tree->priv->accounts) > 0)
		return GBZ_ACCOUNT (tree->priv->accounts->data);
	else
		return NULL;
}

void
gbz_tree_set_active_account (GbzTree    *tree,
			     GbzAccount *account)
{
	g_return_if_fail (tree != NULL);
	g_return_if_fail (GBZ_IS_TREE (tree));
	g_return_if_fail (account != NULL);
	g_return_if_fail (GBZ_IS_ACCOUNT (account));
}

void
gbz_tree_load_config (GbzTree *tree)
{
	char *filename, *bugzilla, *username, *password;
	xmlNodePtr node;
	GbzAccount *account;

	g_return_if_fail (tree != NULL);
	g_return_if_fail (GBZ_IS_TREE (tree));

	if (tree->priv->doc) {
		xmlFreeDoc (tree->priv->doc);
		tree->priv->doc = NULL;
	}

	filename = gnome_util_prepend_user_home (".bugzilla-buddy/accounts.xml");

	if (g_file_test (filename, G_FILE_TEST_IS_REGULAR | G_FILE_TEST_EXISTS)) {
		tree->priv->doc = xmlParseFile (filename);
		if (tree->priv->doc) {
			xmlNodePtr root = tree->priv->doc->children;
			for (node = root->children; node; node = node->next) {
				if (node->type == XML_ELEMENT_NODE) {
					bugzilla = xmlGetProp (node, "bugzilla");
					username = xmlGetProp (node, "username");
					password = xmlGetProp (node, "password");

					load_product_info (tree, bugzilla);

					account = gbz_account_new (bugzilla,
								   username,
								   password);
					gbz_tree_add_account (tree, account);

					gbz_account_load (account);
				}
			}
		}
	}

	g_free (filename);
}

void
gbz_tree_save_config (GbzTree *tree)
{
	char *dir;
	xmlDocPtr doc;
	xmlNodePtr node;
	GSList *l;
	char *bugzilla, *username, *password;
	GSList *visited = NULL;

	g_return_if_fail (tree != NULL);
	g_return_if_fail (GBZ_IS_TREE (tree));

	dir = gnome_util_prepend_user_home (".bugzilla-buddy");
	if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		mkdir (dir, 0755);
		if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
			g_print ("Could not create .bugzilla-buddy directory.\n");
			g_free (dir);
			return;
		}
	}
	g_free (dir);

	/* Save product info. */
	g_hash_table_foreach (tree->priv->product_lists, save_product_info, tree);

	/* Save accounts info. */
	doc = xmlNewDoc ("1.0");
	doc->children = xmlNewDocNode (doc, NULL, "bugzilla-buddy", NULL);

	for (l = tree->priv->accounts; l != NULL; l = l->next) {
		GbzAccount *account = GBZ_ACCOUNT (l->data);
		g_object_get (G_OBJECT (account), "bugzilla", &bugzilla,
			      "username", &username, "password", &password,
			      NULL);

		/* Save account info. */
		node = xmlNewChild (doc->children, NULL, "account", NULL);
		xmlSetProp (node, "bugzilla", bugzilla);
		xmlSetProp (node, "username", username);
		xmlSetProp (node, "password", password);

		/* Save account content. */
		gbz_account_save (account);
	}
	g_slist_free (visited);

	dir = gnome_util_prepend_user_home (".bugzilla-buddy/accounts.xml");
	xmlSaveFormatFile (dir, doc, TRUE);
	g_free (dir);
}

GtkTreeModel *
gbz_tree_get_model (GbzTree *tree)
{
	return tree->priv->tree_model;
}

GSList *
gbz_tree_get_product_list (GbzTree    *tree,
			   const char *bugzilla)
{
	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (GBZ_IS_TREE (tree), NULL);

	return g_hash_table_lookup (tree->priv->product_lists, bugzilla);
}

void
gbz_tree_set_product_list (GbzTree    *tree,
			   const char *bugzilla,
			   GSList     *products)
{
	g_return_if_fail (tree != NULL);
	g_return_if_fail (GBZ_IS_TREE (tree));

	g_hash_table_insert (tree->priv->product_lists, g_strdup (bugzilla),
			     products);
}

GSList *
gbz_tree_get_accounts (GbzTree *tree)
{
	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (GBZ_IS_TREE (tree), NULL);

	return tree->priv->accounts;
}
