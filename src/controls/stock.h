/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_STOCK_H
#define GBZ_STOCK_H

#include <glib/gmacros.h>

#define GNOME_BUGZILLA_STOCK_CONNECT    "bugzilla-buddy-connect"
#define GNOME_BUGZILLA_STOCK_DISCONNECT "bugzilla-buddy-disconnect"
#define GNOME_BUGZILLA_STOCK_DATABASE   "bugzilla-buddy-database"

G_BEGIN_DECLS

/* Add gnome-bugzilla stock icons. */
void stock_init (void);

G_END_DECLS

#endif /* GBZ_STOCK_H */
