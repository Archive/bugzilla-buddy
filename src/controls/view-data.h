/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_VIEW_DATA_H
#define GBZ_VIEW_DATA_H

#include <glib-object.h>

G_BEGIN_DECLS

#define GBF_TYPE_VIEW_DATA	(gbz_view_data_get_type ())

typedef struct _GbzViewData GbzViewData;

typedef enum {
	GBZ_VIEW_NODE_ATTACHMENT,
	GBZ_VIEW_NODE_BUG,
	GBZ_VIEW_NODE_FOLDER,
	GBZ_VIEW_NODE_STRING
} GbzViewNodeType;

struct _GbzViewData {
	GbzViewNodeType type;
	gpointer data;
};

GType        gbz_view_data_get_type (void);
GbzViewData *gbz_view_data_new      (GbzViewNodeType  type,
				     gpointer         data);
GbzViewData *gbz_view_data_copy     (GbzViewData     *data);
void         gbz_view_data_free     (GbzViewData     *data);

G_END_DECLS

#endif /* GBZ_VIEW_DATA_H */
