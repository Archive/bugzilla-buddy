/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_ACCOUNT_H
#define GBZ_ACCOUNT_H

#include <gtk/gtktreeview.h>
#include <libxml/parser.h>

G_BEGIN_DECLS

typedef struct _GbzAccount		GbzAccount;
typedef struct _GbzAccountClass		GbzAccountClass;
typedef struct _GbzAccountPrivate	GbzAccountPrivate;

#define GBZ_TYPE_ACCOUNT        (gbz_account_get_type ())
#define GBZ_ACCOUNT(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GBZ_TYPE_ACCOUNT, GbzAccount))
#define GBZ_ACCOUNT_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), GBZ_TYPE_ACCOUNT, GbzAccountClass))
#define GBZ_IS_ACCOUNT(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GBZ_TYPE_ACCOUNT))
#define GBZ_IS_ACCOUNT_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), GBZ_TYPE_ACCOUNT))

struct _GbzAccount {
	GObject parent;

	GbzAccountPrivate *priv;
};

struct _GbzAccountClass {
	GObjectClass parent_class;
};

/* Creation */
GType              gbz_account_get_type      (void);
GbzAccount        *gbz_account_new           (const char *bugzilla,
					      const char *username,
					      const char *password);

/* Product list methods. */
GHashTable        *gbz_account_get_product_list    (GbzAccount *account);
GHashTable        *gbz_account_update_product_list (GbzAccount *account);

/* Update/save info. */
void               gbz_account_update (GbzAccount *account);

void               gbz_account_load (GbzAccount *account);
void               gbz_account_save (GbzAccount *account);

G_END_DECLS

#endif /* GBZ_ACCOUNT_H */
