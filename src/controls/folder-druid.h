/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_FOLDER_DRUID_H
#define GBZ_FOLDER_DRUID_H

#include <gtk/gtk.h>
#include "tree.h"

G_BEGIN_DECLS

#define GBZ_TYPE_FOLDER_DRUID			(gbz_folder_druid_get_type ())
#define GBZ_FOLDER_DRUID(obj)			(GTK_CHECK_CAST ((obj), GBZ_TYPE_FOLDER_DRUID, GbzFolderDruid))
#define GBZ_FOLDER_DRUID_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GBZ_TYPE_FOLDER_DRUID, GbzFolderDruidClass))
#define GBZ_IS_FOLDER_DRUID(obj)		(GTK_CHECK_TYPE ((obj), GBZ_TYPE_FOLDER_DRUID))
#define GBZ_IS_FOLDER_DRUID_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GBZ_TYPE_FOLDER_DRUID))

typedef struct _GbzFolderDruid			GbzFolderDruid;
typedef struct _GbzFolderDruidPrivate		GbzFolderDruidPrivate;
typedef struct _GbzFolderDruidClass		GbzFolderDruidClass;

struct _GbzFolderDruid {
	GtkWindow parent;

	GbzFolderDruidPrivate *priv;
};

struct _GbzFolderDruidClass {
	GtkWindowClass parent_class;
};

GType                     gbz_folder_druid_get_type (void);
GtkWidget                *gbz_folder_druid_new (GbzTree *tree);

G_END_DECLS

#endif /* GBZ_FOLDER_DRUID_H */
