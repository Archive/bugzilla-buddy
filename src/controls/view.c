/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gconf/gconf-client.h>
#include <glade/glade-xml.h>
#include <libbonoboui.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <string.h>
#include "view.h"
#include "view-data.h"

enum {
	PROP_BOGUS,
	PROP_REPOSITORY
};

struct _GbzViewPrivate {
	GtkWidget *paned;
	GtkWidget *tree_sw;
	GtkWidget *tree;

	GtkWidget *search_option;
	GtkWidget *search_entry;
	GtkWidget *search_button;

	GtkWidget *id_entry;
	GtkWidget *product_entry;
	GtkWidget *version_entry;
	GtkWidget *reporter_entry;
	GtkWidget *component_entry;
	GtkWidget *opsys_entry;
	GtkWidget *summary_entry;
	GtkWidget *comments_view;

	GtkTreeModel *tree_model;

	GbzViewData *empty_node;

	GbzQuery *current_query;
};

/* Prototypes. */
static void gbz_view_finalize (GObject *object);

static void get_property (GObject      *object,
			  guint         prop_id,
			  GValue       *value,
			  GParamSpec   *pspec);
static void set_property (GObject      *object,
			  guint         prop_id,
			  const GValue *value,
			  GParamSpec   *pspec);

static void search_button_cb (GtkWidget *button, gpointer data);
static void cursor_changed_cb (GtkWidget *widget, gpointer data);

static void id_set_text (GtkTreeViewColumn *tree_column,
			 GtkCellRenderer   *cell,
			 GtkTreeModel      *model,
			 GtkTreeIter       *iter,
			 gpointer           data);
static void summary_set_text (GtkTreeViewColumn *tree_column,
			      GtkCellRenderer   *cell,
			      GtkTreeModel      *model,
			      GtkTreeIter       *iter,
			      gpointer           data);

/* Boilerplate. */
GNOME_CLASS_BOILERPLATE (GbzView, gbz_view,
			 GtkVBox, GTK_TYPE_VBOX);

/* Private methods. */
static void
gbz_view_class_init (GbzViewClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	object_class->get_property = get_property;
	object_class->set_property = set_property;
	object_class->finalize = gbz_view_finalize;

	g_object_class_install_property (object_class, PROP_REPOSITORY,
					 g_param_spec_pointer ("repository", 
							       _("Repository"),
							       _("Corba Repository Object"),
							       G_PARAM_READWRITE));
}

static void
gbz_view_instance_init (GbzView *view)
{
	GbzViewPrivate *priv;
	GtkWidget *box, *hbox, *menu, *item, *details;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeIter iter;
	GladeXML *gui;

	priv = g_new0 (GbzViewPrivate, 1);
	view->priv = priv;
	priv->current_query = NULL;

#if 0
	priv->paned = gtk_vpaned_new ();
	box = gtk_vbox_new (FALSE, 0);

	/* Create search box. */
	hbox = gtk_hbox_new (FALSE, 0);
	priv->search_option = gtk_option_menu_new ();
	menu = gtk_menu_new ();
	item = gtk_menu_item_new_with_label (_("Bug ID"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	item = gtk_menu_item_new_with_label (_("Summary contains"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (priv->search_option), menu);
	priv->search_entry = gtk_entry_new ();
	priv->search_button = gtk_button_new_with_label (_("Find Now"));
	g_signal_connect (priv->search_button, "clicked",
			  G_CALLBACK (search_button_cb), view);

	gtk_box_pack_start (GTK_BOX (hbox), priv->search_option, FALSE, FALSE, 2);
	gtk_box_pack_start (GTK_BOX (hbox), priv->search_entry, TRUE, TRUE, 2);
	gtk_box_pack_start (GTK_BOX (hbox), priv->search_button, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box), hbox, FALSE, FALSE, 0);
#endif

	/* Create tree model. */
	priv->tree_model = GTK_TREE_MODEL (gtk_tree_store_new (1, GBF_TYPE_VIEW_DATA));

	/* Create scrolled window. */
	priv->tree_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (priv->tree_sw),
					     GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->tree_sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	/* Create tree view. */
	priv->tree = gtk_tree_view_new_with_model (priv->tree_model);
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (priv->tree), TRUE);

	gtk_container_add (GTK_CONTAINER (priv->tree_sw), priv->tree);
#if 0
	gtk_box_pack_start (GTK_BOX (box), priv->tree_sw, TRUE, TRUE, 0);
#endif
	gtk_box_pack_start (GTK_BOX (view), priv->tree_sw, TRUE, TRUE, 0);

	g_signal_connect (G_OBJECT (priv->tree), "cursor-changed",
			  G_CALLBACK (cursor_changed_cb), view);

	/* Create ID column. */
	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "ID");

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 id_set_text,
						 NULL, NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->tree), column);

	/* Create Summary column. */
	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Summary");

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 summary_set_text,
						 NULL, NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->tree), column);

#if 0
	/* Create details pane. */
	gui = glade_xml_new (DATADIR "/bugzilla-buddy/glade/view.glade", "view-box", NULL);
	if (!gui) {
		g_warning ("Could not find view.glade, reinstall bugzilla-buddy.\n");
		return;
	}

	/* Get widgets. */
	details               = glade_xml_get_widget (gui, "view-box");
	priv->id_entry        = glade_xml_get_widget (gui, "id-entry");
	priv->product_entry   = glade_xml_get_widget (gui, "product-entry");
	priv->version_entry   = glade_xml_get_widget (gui, "version-entry");
	priv->reporter_entry  = glade_xml_get_widget (gui, "reporter-entry");
	priv->component_entry = glade_xml_get_widget (gui, "component-entry");
	priv->opsys_entry     = glade_xml_get_widget (gui, "opsys-entry");
	priv->summary_entry   = glade_xml_get_widget (gui, "summary-entry");
	priv->comments_view   = glade_xml_get_widget (gui, "comments-view");

	/* Add widgets to paned. */
	gtk_paned_add1 (GTK_PANED (priv->paned), box);
	gtk_paned_add2 (GTK_PANED (priv->paned), details);
	gtk_widget_show_all (priv->paned);
	gtk_box_pack_start (GTK_BOX (view), priv->paned, TRUE, TRUE, 0);
#endif

	/* Create empty node. */
	priv->empty_node = gbz_view_data_new (GBZ_VIEW_NODE_STRING, 
					      _("No bugs queried."));
	gtk_tree_store_append (GTK_TREE_STORE (priv->tree_model), 
			       &iter, NULL);
	gtk_tree_store_set (GTK_TREE_STORE (priv->tree_model), &iter,
			    0, priv->empty_node, -1);
}

static void
gbz_view_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GbzView *view = GBZ_VIEW (object);

	switch (prop_id) {
	case PROP_REPOSITORY:
		/*g_value_set_pointer (value, gbz_view_get_repository (view));*/
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
set_property (GObject      *object,
	      guint         prop_id, 
	      const GValue *value,
	      GParamSpec   *pspec)
{
	GbzView *view = GBZ_VIEW (object);

	switch (prop_id) {
	case PROP_REPOSITORY:
		/*gbz_view_set_repository (view, g_value_get_pointer (value));*/
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
search_button_cb (GtkWidget *button, gpointer data)
{
#if 0
	GbzView *view = GBZ_VIEW (data);
	CORBA_Environment ev;
	GNOME_Bugzilla_BugInfoList *bug_list;
	int i;
	GbzViewData *view_data;

	g_return_if_fail (view->repo != CORBA_OBJECT_NIL);

	CORBA_exception_init (&ev);

	bug_list = GNOME_Bugzilla_Repository_query (view->repo, &ev);
	g_assert (!BONOBO_EX (&ev));

	gtk_tree_store_clear (GTK_TREE_STORE (view->priv->tree_model));
	for (i = 0; i < bug_list->_length; i++) {
		GtkTreeIter iter;

		view_data = gbz_view_data_new (GBZ_VIEW_NODE_BUG,
					       &bug_list->_buffer[i]);
		gtk_tree_store_append (GTK_TREE_STORE (view->priv->tree_model), 
				       &iter, NULL);
		gtk_tree_store_set (GTK_TREE_STORE (view->priv->tree_model), &iter,
				    0, view_data, -1);
	}

	CORBA_exception_free (&ev);
#endif
}

static void
cursor_changed_cb (GtkWidget *widget,
		   gpointer   data)
{
#if 0
	GbzView *view;
	GtkTreePath *path;
	GtkTreeIter iter;
	GbzViewData *vdata;

	view = GBZ_VIEW (data);

	gtk_tree_view_get_cursor (GTK_TREE_VIEW (widget), &path, NULL);
	g_return_if_fail (path != NULL);

	gtk_tree_model_get_iter (GTK_TREE_MODEL (view->priv->tree_model),
				 &iter, path);
	gtk_tree_path_free (path);

	gtk_tree_model_get (GTK_TREE_MODEL (view->priv->tree_model),
			    &iter, 0, &vdata, -1);

	if (vdata->type == GBZ_VIEW_NODE_BUG) {
		GNOME_Bugzilla_BugInfo *info;
		GNOME_Bugzilla_Bug *bug;
		CORBA_Environment ev;
		char *value;
		int i;
		GtkTextBuffer *buffer;
		GtkTextIter start, end;

		info = vdata->data;

		CORBA_exception_init (&ev);

		bug = GNOME_Bugzilla_Repository_getBugFromId (view->repo,
							      info->id, &ev);
		g_assert (bug != CORBA_OBJECT_NIL && !BONOBO_EX (&ev));

		CORBA_exception_free (&ev);

		/* Set id_entry field. */
		value = g_strdup_printf ("%d", bug->id);
		gtk_entry_set_text (GTK_ENTRY (view->priv->id_entry), value);
		g_free (value);

		gtk_entry_set_text (GTK_ENTRY (view->priv->product_entry), bug->product);
		gtk_entry_set_text (GTK_ENTRY (view->priv->version_entry), bug->version);
		gtk_entry_set_text (GTK_ENTRY (view->priv->reporter_entry), bug->reporter);
		gtk_entry_set_text (GTK_ENTRY (view->priv->component_entry), bug->component);
		gtk_entry_set_text (GTK_ENTRY (view->priv->opsys_entry), bug->op_sys);
		gtk_entry_set_text (GTK_ENTRY (view->priv->summary_entry), bug->short_desc);

		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view->priv->comments_view));
		gtk_text_buffer_get_start_iter (buffer, &start);
		gtk_text_buffer_get_end_iter (buffer, &end);
		gtk_text_buffer_delete (buffer, &start, &end);

		for (i = 0; i < bug->long_desc._length; i++) {
			GNOME_Bugzilla_Entry entry = bug->long_desc._buffer[i];
			char *entry_text = g_strdup_printf ("%s, %s\n%s\n\n",
							    entry.user,
							    entry.date,
							    entry.text);

			gtk_text_buffer_insert_at_cursor (buffer, entry_text,
							  strlen (entry_text));
			g_free (entry_text);
		}
	}
#endif
}

static void
id_set_text (GtkTreeViewColumn *tree_column,
	     GtkCellRenderer   *cell,
	     GtkTreeModel      *model,
	     GtkTreeIter       *iter,
	     gpointer           data)
{
#if 0
	GbzViewData *tdata;
	char *str;
	GNOME_Bugzilla_BugInfo *bug_info;

	gtk_tree_model_get (model, iter, 0, &tdata, -1);

	switch (tdata->type) {
	case GBZ_VIEW_NODE_BUG:
		bug_info = tdata->data;
		str = g_strdup_printf ("%d", bug_info->id);
		g_object_set (GTK_CELL_RENDERER (cell), "text",
			      str, NULL);
		g_free (str);
		break;
	case GBZ_VIEW_NODE_ATTACHMENT:
	case GBZ_VIEW_NODE_FOLDER:
	case GBZ_VIEW_NODE_STRING:
		g_object_set (GTK_CELL_RENDERER (cell), "text", 
			      tdata->data, NULL);
		break;
	default:
		g_object_set (GTK_CELL_RENDERER (cell), "text", NULL, NULL);
	}

	gbz_view_data_free (tdata);
#endif
}

static void
summary_set_text (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer   *cell,
		  GtkTreeModel      *model,
		  GtkTreeIter       *iter,
		  gpointer           data)
{
#if 0
	GbzViewData *tdata;
	GNOME_Bugzilla_BugInfo *bug_info;

	gtk_tree_model_get (model, iter, 0, &tdata, -1);

	switch (tdata->type) {
	case GBZ_VIEW_NODE_BUG:
		bug_info = tdata->data;
		g_object_set (GTK_CELL_RENDERER (cell), "text",
			      bug_info->summary, NULL);
		break;
	default:
		g_object_set (GTK_CELL_RENDERER (cell), "text", NULL, NULL);
	}

	gbz_view_data_free (tdata);
#endif
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GtkWidget *
gbz_view_new (void)
{	GbzView *view;

	view = GBZ_VIEW (g_object_new (GBZ_TYPE_VIEW, NULL));

	return GTK_WIDGET (view);
}

void
gbz_view_set_query (GbzView  *view,
		    GbzQuery *query)
{
	BonoboUIComponent *uic;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GBZ_IS_VIEW (view));
	g_return_if_fail (query != NULL);
	g_return_if_fail (GBZ_IS_QUERY (query));

	if (view->priv->current_query)
		g_object_unref (view->priv->current_query);

	view->priv->current_query = query;

	/* Update UI. */
	uic = g_object_get_data (G_OBJECT (view), "ui_component");
	bonobo_ui_component_set_prop (uic, "/commands/RefreshQuery",
				      "sensitive", "1", NULL);
}

GbzQuery *
gbz_view_get_query (GbzView *view)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GBZ_IS_VIEW (view));

	return view->priv->current_query;
}
