/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <libgnome/gnome-util.h>
#include <libxml/tree.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include "account.h"
#include "tree-data.h"

enum {
	PROP_BOGUS,
	PROP_BUGZILLA,
	PROP_USERNAME,
	PROP_PASSWORD,
	PROP_PRODUCT_LIST,
	PROP_TREE_MODEL,
	PROP_TREE_ITER
};

struct _GbzAccountPrivate {
	char *bugzilla;
	char *username;
	char *password;
	GHashTable *product_list;

	GtkTreeModel *tree_model;
	GtkTreeIter *tree_iter;
};

/* Prototypes. */
static void gbz_account_finalize (GObject *object);
static void gbz_account_get_property (GObject    *object,
				      guint       prop_id,
				      GValue     *value,
				      GParamSpec *pspec);
static void gbz_account_set_property (GObject      *object,
				      guint         prop_id,
				      const GValue *value,
				      GParamSpec   *pspec);

/* Boilerplate code. */
GNOME_CLASS_BOILERPLATE (GbzAccount, gbz_account,
			 GObject, G_TYPE_OBJECT);

/* Private routines */
static void
gbz_account_class_init (GbzAccountClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_account_finalize;
	object_class->get_property = gbz_account_get_property;
	object_class->set_property = gbz_account_set_property;

	g_object_class_install_property (object_class,
					 PROP_BUGZILLA,
					 g_param_spec_string ("bugzilla",
							      _("Bugzilla server"),
							      _("Address of the bugzilla server."),
							      "",
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_USERNAME,
					 g_param_spec_string ("username",
						 	      _("Bugzilla username"),
							      _("The username of the bugzilla account."),
							      "",
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_PASSWORD,
					 g_param_spec_string ("password",
						 	      _("Bugzilla password"),
							      _("The password of the bugzilla account."),
							      "",
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_PRODUCT_LIST,
					 g_param_spec_pointer ("product-list",
							       _("Bugzilla product list"),
							       _("The list of products stored on the server."),
							       G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_TREE_MODEL,
					 g_param_spec_pointer ("tree-model",
							       _("Tree model"),
							       _("Tree model of accounts tree."),
							       G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_TREE_ITER,
					 g_param_spec_pointer ("tree-iter",
							       _("Tree iter"),
							       _("Tree iter of account node."),
							       G_PARAM_READWRITE));
}

static void
gbz_account_instance_init (GbzAccount *account)
{
	account->priv = g_new0 (GbzAccountPrivate, 1);
}

static void
gbz_account_get_property (GObject    *object,
			  guint       prop_id,
			  GValue     *value,
			  GParamSpec *pspec)
{
	GbzAccount *account = GBZ_ACCOUNT (object);

	switch (prop_id) {
		case PROP_BUGZILLA:
			g_value_set_string (value, account->priv->bugzilla);
			break;
		case PROP_USERNAME:
			g_value_set_string (value, account->priv->username);
			break;
		case PROP_PASSWORD:
			g_value_set_string (value, account->priv->password);
			break;
		case PROP_PRODUCT_LIST:
			g_value_set_pointer (value, account->priv->product_list);
			break;
		case PROP_TREE_MODEL:
			g_value_set_pointer (value, account->priv->tree_model);
			break;
		case PROP_TREE_ITER:
			g_value_set_pointer (value, account->priv->tree_iter);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
gbz_account_set_property (GObject      *object,
			  guint         prop_id,
			  const GValue *value,
			  GParamSpec   *pspec)
{
	GbzAccount *account = GBZ_ACCOUNT (object);

	switch (prop_id) {
		case PROP_BUGZILLA:
			if (account->priv->bugzilla)
				g_free (account->priv->bugzilla);
			account->priv->bugzilla = g_strdup (g_value_get_string (value));
			break;
		case PROP_USERNAME:
			if (account->priv->username)
				g_free (account->priv->username);
			account->priv->username = g_strdup (g_value_get_string (value));
			break;
		case PROP_PASSWORD:
			if (account->priv->password)
				g_free (account->priv->password);
			account->priv->password = g_strdup (g_value_get_string (value));
			break;
		case PROP_PRODUCT_LIST:
			account->priv->product_list = g_value_get_pointer (value);
			break;
		case PROP_TREE_MODEL:
			account->priv->tree_model = GTK_TREE_MODEL (g_value_get_pointer (value));
			break;
		case PROP_TREE_ITER:
			account->priv->tree_iter = g_value_get_pointer (value);
			break;
		default:
			break;
	}
}

static void
gbz_account_finalize (GObject *object)
{
}

static void
load_folders (GbzAccount  *account,
	      GtkTreeIter *iter,
	      xmlNodePtr   node)
{
	GbzAccountPrivate *priv = account->priv;
	GtkTreeIter child_iter;
	GbzTreeData *tdata;
	char *name;
	xmlNodePtr child;

	if (!strcmp ("folder", node->name)) {
		name = xmlGetProp (node, "name");
		tdata = gbz_tree_data_new (GBZ_TREE_NODE_FOLDER,
					   g_strdup (name));

		gtk_tree_store_append (GTK_TREE_STORE (priv->tree_model),
				       &child_iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (priv->tree_model),
				    &child_iter, 0, tdata, -1);

		for (child = node->children; child; child = child->next) {
			load_folders (account, &child_iter, child);
		}
	} else if (!strcmp ("query", node->name)) {
		name = xmlGetProp (node, "name");
		tdata = gbz_tree_data_new (GBZ_TREE_NODE_QUERY,
					   g_strdup (name));

		gtk_tree_store_append (GTK_TREE_STORE (priv->tree_model),
				       &child_iter, iter);
		gtk_tree_store_set (GTK_TREE_STORE (priv->tree_model),
				    &child_iter, 0, tdata, -1);
	} else if (!strcmp ("folder-list", node->name)) {
		for (child = node->children; child; child = child->next) {
			load_folders (account, iter, child);
		}
	}
}

static void
save_folders (GbzAccount  *account,
	      GtkTreeIter *iter,
	      xmlNodePtr   node)
{
	GbzAccountPrivate *priv = account->priv;
	GbzTreeData *data;
	GtkTreeIter child_iter;
	xmlNodePtr child_node;

	gtk_tree_model_get (priv->tree_model, iter, 0, &data, -1);
	switch (data->type) {
	case GBZ_TREE_NODE_ACCOUNT:
		break;
	case GBZ_TREE_NODE_QUERY:
		child_node = xmlNewChild (node, NULL, "query", NULL);
		xmlSetProp (child_node, "name", data->data);
		break;
	case GBZ_TREE_NODE_FOLDER:
		child_node = xmlNewChild (node, NULL, "folder", NULL);
		xmlSetProp (child_node, "name", data->data);

		gtk_tree_model_iter_children (priv->tree_model, &child_iter, iter);
		do {
			save_folders (account, &child_iter, child_node);
		} while (gtk_tree_model_iter_next (priv->tree_model, &child_iter));
		break;
	}
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GbzAccount *
gbz_account_new (const char *bugzilla,
		 const char *username,
		 const char *password)
{
	GbzAccount *account;

	g_return_val_if_fail (bugzilla != NULL, NULL);
	/*g_return_val_if_fail (product_list != NULL, NULL);*/

	account = GBZ_ACCOUNT (g_object_new (GBZ_TYPE_ACCOUNT,
					     "bugzilla", bugzilla,
					     "username", username,
					     "password", password,
					     NULL));

	return account;
}

GHashTable *
gbz_account_get_product_list (GbzAccount *account)
{
	g_return_val_if_fail (account != NULL, NULL);
	g_return_val_if_fail (GBZ_IS_ACCOUNT (account), NULL);

	return account->priv->product_list;
}

GHashTable *
gbz_account_update_product_list (GbzAccount *account)
{
	g_return_val_if_fail (account != NULL, NULL);
	g_return_val_if_fail (GBZ_IS_ACCOUNT (account), NULL);

	g_print ("implement gbz_account_update_product_list\n");

	return NULL;
}

void
gbz_account_update (GbzAccount *account)
{
}

void
gbz_account_load (GbzAccount *account)
{
	char *temp, *dir, *filename;
	xmlDocPtr doc;
	xmlNodePtr node;

	g_return_if_fail (account != NULL);
	g_return_if_fail (GBZ_IS_ACCOUNT (account));

	/* Check if account dir exists. */
	temp = g_strconcat (".bugzilla-buddy/", account->priv->bugzilla, "/",
			    account->priv->username, NULL);
	dir = gnome_util_prepend_user_home (temp);
	g_free (temp);
	if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		g_print ("Could not load account data: %s\n", account->priv->username);
		g_free (dir);
		return;
	}

	filename = g_strconcat (dir, "/folders.xml", NULL);
	g_free (dir);

	/* Load folders.xml. */
	doc = xmlParseFile (filename);
	g_free (filename);
	if (!doc) {
		g_print ("Error loading account folders\n");
		return;
	}

	/* Load folders. */
	for (node = doc->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE)
			load_folders (account, account->priv->tree_iter, node);
	}
}

void
gbz_account_save (GbzAccount *account)
{
	char *temp, *dir, *filename;
	xmlDocPtr doc;
	GtkTreeIter iter;

	g_return_if_fail (account != NULL);
	g_return_if_fail (GBZ_IS_ACCOUNT (account));

	/* Create directory for this account. */
	temp = g_strconcat (".bugzilla-buddy/", account->priv->bugzilla, "/",
			    account->priv->username, NULL);
	dir = gnome_util_prepend_user_home (temp);
	g_free (temp);
	if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		mkdir (dir, 0755);
		if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
			g_print ("Could not create .bugzilla-buddy/%s/%s directory.\n",
				 account->priv->bugzilla, account->priv->username);
			g_free (dir);
			return;
		}
	}

	/* Save folder data to folders.xml. */
	filename = g_strconcat (dir, "/folders.xml", NULL);
	g_free (dir);

	doc = xmlNewDoc ("1.0");
	doc->children = xmlNewDocNode (doc, NULL, "folder-list", NULL);

	if (gtk_tree_model_iter_children (account->priv->tree_model, &iter,
					  account->priv->tree_iter)) {
		do {
			save_folders (account, &iter, doc->children);
		} while (gtk_tree_model_iter_next (account->priv->tree_model, &iter));
	}

	xmlSaveFormatFile (filename, doc, TRUE);
	g_free (filename);
}
