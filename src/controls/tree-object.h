/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_TREE_OBJECT_H
#define GBZ_TREE_OBJECT_H

#include <bonobo/bonobo-object.h>
#include "GNOME_Bugzilla.h"
#include "tree.h"

G_BEGIN_DECLS

#define GBZ_TYPE_TREE_OBJECT		(gbz_tree_object_get_type ())
#define GBZ_TREE_OBJECT(obj)		(GTK_CHECK_CAST ((obj), GBZ_TYPE_TREE_OBJECT, GbzTreeObject))
#define GBZ_TREE_OBJECT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GBZ_TYPE_TREE_OBJECT, GbzTreeObjectClass))
#define GBZ_IS_TREE_OBJECT(obj)		(GTK_CHECK_TYPE ((obj), GBZ_TYPE_TREE_OBJECT))
#define GBZ_IS_TREE_OBJECT_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GBZ_TYPE_TREE_OBJECT))
typedef struct _GbzTreeObject		GbzTreeObject;

typedef struct _GbzTreeObjectPrivate	GbzTreeObjectPrivate;
typedef struct _GbzTreeObjectClass	GbzTreeObjectClass;

struct _GbzTreeObject {
	BonoboObject parent;

	GbzTreeObjectPrivate *priv;
};

struct _GbzTreeObjectClass {
	BonoboObjectClass parent_class;

	POA_GNOME_Bugzilla_FolderTree__epv epv;
};

/* Creation. */
GType          gbz_tree_object_get_type        (void);
GbzTreeObject *gbz_tree_object_new             (GbzTree *tree);

G_END_DECLS

#endif /* GBZ_TREE_OBJECT_H */
