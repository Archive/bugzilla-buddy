/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <libgnomeui/gnome-druid.h>
#include <libgnomeui/gnome-druid-page-standard.h>
#include <libgnomevfs/gnome-vfs.h>
#include <string.h>
#include <regex.h>
#include "account-druid.h"

#define BUF_SIZE 2048

enum {
	PRODUCT_NAME,
	PRODUCT_DATA,

	PRODUCT_COLS
};

struct _GbzAccountDruidPrivate {
	GnomeDruidPage		*intro_page;
	GnomeDruidPage		*server_page;
	GnomeDruidPage		*check_page;
	GtkWidget		*window;

	/* Server page. */
	GtkWidget		*bugzilla_entry;
	GtkWidget		*username_entry;
	GtkWidget		*password_entry;

	/* Finish page. */
	GtkWidget		*status_label;
	GtkWidget		*progress;
	GtkWidget		*error_label;
	GtkWidget		*button_hbox;
	GtkWidget		*add_button;

	gchar			*query_page;

	char			*bugzilla;
	char			*username;
	char			*password;
	GHashTable		*products;

	GbzTree			*tree;
};

/* Prototypes. */
static void gbz_account_druid_finalize (GObject *object);

static GnomeDruidPage *create_intro_page (GbzAccountDruid *druid);
static GnomeDruidPage *create_server_page (GbzAccountDruid *druid);
static GnomeDruidPage *create_check_page (GbzAccountDruid *druid);

static gboolean intro_prepare_cb (GnomeDruidPage *page,
				  GtkWidget      *widget,
				  gpointer        data);
static gboolean server_next_cb (GnomeDruidPage *page,
				GtkWidget      *widget,
				gpointer        data);
static gboolean check_prepare_cb (GnomeDruidPage *page,
				  GtkWidget      *widget,
				  gpointer        data);
static gboolean check_finish_cb (GnomeDruidPage *page,
				 GtkWidget      *widget,
				 gpointer        data);

/* Boilerplate code. */
GNOME_CLASS_BOILERPLATE (GbzAccountDruid, gbz_account_druid,
			 GnomeDruid, GNOME_TYPE_DRUID);

/* Private routines */
static void
gbz_account_druid_class_init (GbzAccountDruidClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_account_druid_finalize;
}

static void
gbz_account_druid_instance_init (GbzAccountDruid *druid)
{
	druid->priv = g_new0 (GbzAccountDruidPrivate, 1);
	druid->priv->products = g_hash_table_new (g_str_hash, g_str_equal);
	druid->priv->error_label = NULL;

	druid->priv->intro_page = create_intro_page (druid);
	druid->priv->server_page = create_server_page (druid);
	druid->priv->check_page = create_check_page (druid);

	gnome_druid_append_page (GNOME_DRUID (druid), druid->priv->intro_page);
	gnome_druid_append_page (GNOME_DRUID (druid), druid->priv->server_page);
	gnome_druid_append_page (GNOME_DRUID (druid), druid->priv->check_page);
	gnome_druid_set_show_help (GNOME_DRUID (druid), TRUE);
	gnome_druid_set_show_finish (GNOME_DRUID (druid), FALSE);

	g_signal_connect_after (G_OBJECT (druid->priv->intro_page), "prepare",
				G_CALLBACK (intro_prepare_cb), druid);
	g_signal_connect (G_OBJECT (druid->priv->server_page), "next",
			  G_CALLBACK (server_next_cb), druid);
	g_signal_connect_after (G_OBJECT (druid->priv->check_page), "prepare",
				G_CALLBACK (check_prepare_cb), druid);
	g_signal_connect (G_OBJECT (druid->priv->check_page), "finish",
				G_CALLBACK (check_finish_cb), druid);

	gtk_widget_show_all (GTK_WIDGET (druid));
}

static void
gbz_account_druid_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GnomeDruidPage *
create_intro_page (GbzAccountDruid *druid)
{
	GtkWidget *page, *label;

	page = gnome_druid_page_standard_new_with_vals (_("New Bugzilla Account"),
							NULL, NULL);

	label = gtk_label_new (_("This druid allows you to add a bugzilla account"
				 " to bugzilla-buddy. If you want to be able to "
				 "modify bugs for this bugzilla server, you need "
				 "to have an existing account, otherwise you will "
				 "only be able to view bugs.\n\nPress Forward to "
				 "start."));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);

	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    label, FALSE, FALSE, 0);

	return GNOME_DRUID_PAGE (page);
}

static GnomeDruidPage *
create_server_page (GbzAccountDruid *druid)
{
	GtkWidget *page, *label, *table;

	page = gnome_druid_page_standard_new_with_vals (_("Enter Server"),
							NULL, NULL);

	label = gtk_label_new (_("Enter the HTTP address where the bugzilla server "
				 "resides. The server will be checked whether it "
				 "is compatible with bugzilla-buddy. Leave the "
				 "e-mail and password fields empty if you only "
				 "want to view bugs and not modify them."));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);

	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    label, FALSE, FALSE, 0);

	table = gtk_table_new (3, 2, FALSE);
	label = gtk_label_new (_("_Server:"));
	gtk_label_set_use_underline (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.0);
	druid->priv->bugzilla_entry = gtk_entry_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), druid->priv->bugzilla_entry);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, 0, 5, 5);
	gtk_table_attach (GTK_TABLE (table), druid->priv->bugzilla_entry,
			  1, 2, 0, 1, GTK_FILL | GTK_EXPAND, 0, 0, 5);

	label = gtk_label_new (_("_E-mail:"));
	gtk_label_set_use_underline (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.0);
	druid->priv->username_entry = gtk_entry_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), druid->priv->username_entry);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, 0, 5, 5);
	gtk_table_attach (GTK_TABLE (table), druid->priv->username_entry,
			  1, 2, 1, 2, GTK_FILL | GTK_EXPAND, 0, 0, 5);

	label = gtk_label_new (_("_Password:"));
	gtk_label_set_use_underline (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.0);
	druid->priv->password_entry = gtk_entry_new ();
	gtk_entry_set_visibility (GTK_ENTRY (druid->priv->password_entry), FALSE);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), druid->priv->password_entry);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_FILL, 0, 5, 5);
	gtk_table_attach (GTK_TABLE (table), druid->priv->password_entry,
			  1, 2, 2, 3, GTK_FILL | GTK_EXPAND, 0, 0, 5);

	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    table, FALSE, FALSE, 0);

	return GNOME_DRUID_PAGE (page);
}

static GnomeDruidPage *
create_check_page (GbzAccountDruid *druid)
{
	GbzAccountDruidPrivate *priv = druid->priv;
	GtkWidget *page;

	page = gnome_druid_page_standard_new_with_vals (_("Retrieving Information"),
							NULL, NULL);

	priv->status_label = gtk_label_new (_("Connecting to server..."));
	gtk_label_set_line_wrap (GTK_LABEL (priv->status_label), TRUE);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    priv->status_label, FALSE, FALSE, 0);

	priv->progress = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox),
			    priv->progress, FALSE, FALSE, 0);

	return GNOME_DRUID_PAGE (page);
}

static void
parse_product_info (GbzAccountDruid *druid)
{
	regex_t reg;
	regmatch_t matches;
	int i = 0;
	char *match, *name, *start, *end;
	GbzProductInfo *info;
	GSList *items = NULL;

	const char *PRODUCT_REGEXP = "cpts\\['[^']+'\\] = [^;]+;";
	const char *VERSION_REGEXP = "vers\\['[^']+'\\] = [^;]+;";
	const char *MILESTONE_REGEXP = "tms\\['[^']+'\\] = [^;]+;";

	/* Parse & create product list. */
	if (!regcomp (&reg, PRODUCT_REGEXP, REG_EXTENDED)) {
		while (!regexec (&reg, druid->priv->query_page + i, 1, &matches, 0)) {
			match = g_strndup (druid->priv->query_page + i + matches.rm_so,
					   matches.rm_eo - matches.rm_so);
			i += matches.rm_eo;

			/* Create new GbzProductInfo. */
			name = g_strndup (match + 6, index (match + 6, '\'') - (match + 6));
			info = g_new0 (GbzProductInfo, 1);
			info->name = name;
			g_hash_table_insert (druid->priv->products, name, info);

			/* Add components to product. */
			start = index (match, '=');
			while (TRUE) {
				start = index (start, '\'');
				if (!start)
					break;
				end = index (start + 1, '\'');
				if ((end - 1)[0] == '\\')
					end = index (end + 1, '\'');
				name = g_strndup (start + 1, end - start - 1);
				start = end + 1;
				items = g_slist_append (items, name);
			}
			info->components = items;
			items = NULL;
			g_free (match);
		}
	}

	i = 0;
	/* Parse & add version info to product list. */
	if (!regcomp (&reg, VERSION_REGEXP, REG_EXTENDED)) {
		while (!regexec (&reg, druid->priv->query_page + i, 1, &matches, 0)) {
			match = g_strndup (druid->priv->query_page + i + matches.rm_so,
					   matches.rm_eo - matches.rm_so);
			i += matches.rm_eo;

			/* Retrieve BugzillaProduct from hashtable. */
			name = g_strndup (match + 6, index (match + 6, '\'') - (match + 6));
			info = g_hash_table_lookup (druid->priv->products, name);
			g_free (name);

			/* Add versions to product. */
			start = index (match, '=');
			while (TRUE) {
				start = index (start, '\'');
				if (!start)
					break;
				end = index (start + 1, '\'');
				if ((end - 1)[0] == '\\')
					end = index (end + 1, '\'');
				name = g_strndup (start + 1, end - start - 1);
				start = end + 1;
				items = g_slist_append (items, name);
			}
			info->versions = items;
			items = NULL;
			g_free (match);
		}
	}

	i = 0;
	/* Parse & add milestone info to product list. */
	if (!regcomp (&reg, MILESTONE_REGEXP, REG_EXTENDED)) {
		while (!regexec (&reg, druid->priv->query_page + i, 1, &matches, 0)) {
			match = g_strndup (druid->priv->query_page + i + matches.rm_so,
					   matches.rm_eo - matches.rm_so);
			i += matches.rm_eo;

			/* Retrieve BugzillaProduct from hashtable. */
			name = g_strndup (match + 5, index (match + 5, '\'') - (match + 5));
			info = g_hash_table_lookup (druid->priv->products, name);
			g_free (name);

			/* Add milestones to product. */
			start = index (match, '=');
			while (TRUE) {
				start = index (start, '\'');
				if (!start)
					break;
				end = index (start + 1, '\'');
				if ((end - 1)[0] == '\\')
					end = index (end + 1, '\'');
				name = g_strndup (start + 1, end - start - 1);
				start = end + 1;
				items = g_slist_append (items, name);
			}
			info->milestones = items;
			items = NULL;
			g_free (match);
		}
	}

	g_free (druid->priv->query_page);
}

static void
add_product_cb (GtkWidget *widget,
		gpointer   data)
{
	g_print ("add_product\n");
}

static void
bugzilla_close_cb (GnomeVFSAsyncHandle *handle,
		   GnomeVFSResult       result,
		   gpointer             data)
{
	GbzAccountDruid *druid = GBZ_ACCOUNT_DRUID (data);
	regex_t reg;
	regmatch_t matches;
	char *match;

	const char *TITLE_REGEXP = "<TITLE>[^<]+</TITLE>";
	const char *INVALID_EMAIL = "Check e-mail syntax";
	const char *LOGIN_FAILED = "Login failed";

	/* Check the return value from the login. */
	if (!regcomp (&reg, TITLE_REGEXP, REG_EXTENDED)) {
		if (!regexec (&reg, druid->priv->query_page, 1, &matches, 0)) {
			match = g_strndup (druid->priv->query_page + matches.rm_so + 7,
					   matches.rm_eo - matches.rm_so - 15);
			if (!strcmp (match, INVALID_EMAIL)) {
				druid->priv->error_label = gtk_label_new (
					_("<b>Invalid e-mail address. A legal address"
					  " must contain exactly one '@', and at "
					  "least one '.' after the @, and may not"
					  " contain any commas or spaces. Please "
					  "click Back and try again.</b>"));
				gtk_label_set_line_wrap (GTK_LABEL (druid->priv->error_label), TRUE);
				gtk_label_set_use_markup (GTK_LABEL (druid->priv->error_label), TRUE);
				gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
						    druid->priv->error_label, FALSE, FALSE, 0);
				gtk_widget_show (druid->priv->error_label);
			} else if (!strcmp (match, LOGIN_FAILED)) {
				druid->priv->error_label = gtk_label_new (
					_("<b>The username or password you entered "
					  "is not valid. Please click Back and "
					  "try again.</b>"));
				gtk_label_set_line_wrap (GTK_LABEL (druid->priv->error_label), TRUE);
				gtk_label_set_use_markup (GTK_LABEL (druid->priv->error_label), TRUE);
				gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
						    druid->priv->error_label, FALSE, FALSE, 0);
				gtk_widget_show (druid->priv->error_label);
			} else {
				/* Parse the product information from query.cgi. */
				parse_product_info (druid);

				/* Display finish message. */
				gtk_container_remove (GTK_CONTAINER (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
						      druid->priv->status_label);
				gtk_container_remove (GTK_CONTAINER (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
						      druid->priv->progress);
				druid->priv->error_label = gtk_label_new (
					_("The product information from the bugzilla "
					  "server has been retrieved and stored. "
					  "The next step is to add one or more "
					  "products. Please click the Add Product "
					  "button if you want to do so now, otherwise "
					  "click on the Apply button."));
				gtk_label_set_line_wrap (GTK_LABEL (druid->priv->error_label), TRUE);
				gtk_label_set_use_markup (GTK_LABEL (druid->priv->error_label), TRUE);
				gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
						    druid->priv->error_label, FALSE, FALSE, 0);
				gtk_widget_show (druid->priv->error_label);
				druid->priv->button_hbox = gtk_hbox_new (FALSE, 0);
				druid->priv->add_button = gtk_button_new_with_mnemonic (_("Add _Product"));
				g_signal_connect (G_OBJECT (druid->priv->add_button),
						  "clicked", G_CALLBACK (add_product_cb),
						  druid);
				gtk_box_pack_end (GTK_BOX (druid->priv->button_hbox),
						  druid->priv->add_button, FALSE, FALSE, 0);
				gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
						    druid->priv->button_hbox, FALSE, FALSE, 0);
				gtk_widget_show_all (druid->priv->button_hbox);
				gtk_widget_set_sensitive (GNOME_DRUID (druid)->finish, TRUE);
			}
			g_free (match);
		}
	}
}

static void
bugzilla_read_cb (GnomeVFSAsyncHandle *handle,
		  GnomeVFSResult       result,
		  gpointer             buffer,
		  GnomeVFSFileSize     bytes_requested,
		  GnomeVFSFileSize     bytes_read,
		  gpointer             data)
{
	GbzAccountDruid *druid = GBZ_ACCOUNT_DRUID (data);
	static char buf[BUF_SIZE];
	char *text, *bla;

	if (result == GNOME_VFS_OK) {
		text = g_strndup (buffer, bytes_read);
		bla = g_strconcat (druid->priv->query_page, text, NULL);
		if (druid->priv->query_page && strlen (druid->priv->query_page) > 0)
			g_free (druid->priv->query_page);
		druid->priv->query_page = bla;
		g_free (text);

		gtk_progress_bar_pulse (GTK_PROGRESS_BAR (druid->priv->progress));

		gnome_vfs_async_read (handle, buf, BUF_SIZE, bugzilla_read_cb, data);
	} else if (result == GNOME_VFS_ERROR_EOF) {
		gnome_vfs_async_close (handle, bugzilla_close_cb, data);
	} else {
		char *msg = g_strdup_printf (_("<b>Unable to retrieve information from server: %s.</b>"),
					     gnome_vfs_result_to_string (result));
		druid->priv->error_label = gtk_label_new (msg);
		g_free (msg);
		gtk_label_set_line_wrap (GTK_LABEL (druid->priv->error_label), TRUE);
		gtk_label_set_use_markup (GTK_LABEL (druid->priv->error_label), TRUE);
		gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
				    druid->priv->error_label, FALSE, FALSE, 0);
		gtk_widget_show (druid->priv->error_label);
		gnome_vfs_async_close (handle, bugzilla_close_cb, data);
	}
}

static void
bugzilla_open_cb (GnomeVFSAsyncHandle *handle,
		  GnomeVFSResult       result,
		  gpointer             data)
{
	GbzAccountDruid *druid = GBZ_ACCOUNT_DRUID (data);
	char buffer[BUF_SIZE];

	if (result != GNOME_VFS_OK) {
		char *msg = g_strdup_printf (_("<b>Unable to connect to server: %s.</b>"),
					     gnome_vfs_result_to_string (result));
		druid->priv->error_label = gtk_label_new (msg);
		g_free (msg);
		gtk_label_set_line_wrap (GTK_LABEL (druid->priv->error_label), TRUE);
		gtk_label_set_use_markup (GTK_LABEL (druid->priv->error_label), TRUE);
		gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
				    druid->priv->error_label, FALSE, FALSE, 0);
		gtk_widget_show (druid->priv->error_label);
		return;
	}

	druid->priv->query_page = "";
	gtk_label_set_label (GTK_LABEL (druid->priv->status_label),
			     _("Retrieving product information..."));
	gnome_vfs_async_read (handle, buffer, BUF_SIZE, bugzilla_read_cb, data);
}

static void
check_server (GbzAccountDruid *druid)
{
	GnomeVFSHandle *handle;

	if (!gnome_vfs_initialized ())
		gnome_vfs_init ();

	/* Check that the bugzilla address does not start with http://. */
	if (strstr (druid->priv->bugzilla, "http://") == druid->priv->bugzilla) {
		char *temp = g_strdup (druid->priv->bugzilla + 7);
		g_free (druid->priv->bugzilla);
		druid->priv->bugzilla = temp;
	}

	/* Check username and either login or just go to the query page directly. */
	if (!druid->priv->username && strlen (druid->priv->username) == 0) {
		char *address = g_strconcat ("http://", druid->priv->bugzilla,
					     "/query.cgi", NULL);
		gnome_vfs_async_open (&handle, address, GNOME_VFS_OPEN_READ, 0,
				      bugzilla_open_cb, druid);
		g_free (address);
	} else {
		char *address = g_strconcat ("http://", druid->priv->bugzilla,
					     "/query.cgi?Bugzilla_login=", druid->priv->username,
					     "&Bugzilla_password=", druid->priv->password,
					     "&GoAheadAndLogIn=1'", NULL);
		gnome_vfs_async_open (&handle, address, GNOME_VFS_OPEN_READ, 0,
				      bugzilla_open_cb, druid);
		g_free (address);
	}
}

static gboolean
intro_prepare_cb (GnomeDruidPage *page,
		  GtkWidget      *widget,
		  gpointer        data)
{
	gnome_druid_set_buttons_sensitive (GNOME_DRUID (widget), FALSE, TRUE,
					   TRUE, TRUE);

	return TRUE;
}

static gboolean
server_next_cb (GnomeDruidPage *page,
		GtkWidget      *widget,
		gpointer        data)
{
	GbzAccountDruid *druid = GBZ_ACCOUNT_DRUID (data);
	GtkWidget *dialog;
	const char *bugzilla, *username, *password;

	bugzilla = gtk_entry_get_text (GTK_ENTRY (druid->priv->bugzilla_entry));
	username = gtk_entry_get_text (GTK_ENTRY (druid->priv->username_entry));
	password = gtk_entry_get_text (GTK_ENTRY (druid->priv->password_entry));

	/* Check if the user has entered an address. */
	if (bugzilla == NULL || strlen (bugzilla) == 0) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (druid->priv->window),
						 GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_INFO,
						 GTK_BUTTONS_CLOSE,
						 _("You need to enter the address of a bugzilla server."));
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		return TRUE;
	}

	druid->priv->bugzilla = g_strdup (bugzilla);
	druid->priv->username = g_strdup (username);
	druid->priv->password = g_strdup (password);

	return FALSE;
}

static gboolean
check_prepare_cb (GnomeDruidPage *page,
		  GtkWidget      *widget,
		  gpointer        data)
{
	GbzAccountDruid *druid = GBZ_ACCOUNT_DRUID (data);

	/* Update button status. */
	gnome_druid_set_show_finish (GNOME_DRUID (druid), TRUE);
	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), TRUE, FALSE,
					   TRUE, TRUE);
	gtk_widget_set_sensitive (GNOME_DRUID (druid)->finish, FALSE);

	/* Remove error label if visible. */
	if (druid->priv->error_label)
		gtk_container_remove (GTK_CONTAINER (GNOME_DRUID_PAGE_STANDARD (druid->priv->check_page)->vbox),
				      druid->priv->error_label);

	/* Verify server is compatible & retrieve product information. */
	check_server (druid);

	return TRUE;
}

static void
copy_product (gpointer key,
	      gpointer value,
	      gpointer data)
{
	GSList **products = data;
	*products = g_slist_append (*products, value);
}

static gboolean
check_finish_cb (GnomeDruidPage *page,
		 GtkWidget      *widget,
		 gpointer        data)
{
	GbzAccountDruid *druid = GBZ_ACCOUNT_DRUID (data);
	GbzAccount *account;
	GSList *product_list = NULL;

	g_hash_table_foreach (druid->priv->products, copy_product, &product_list);
	gbz_tree_set_product_list (druid->priv->tree, druid->priv->bugzilla,
				   product_list);

	account = gbz_account_new (druid->priv->bugzilla,
				   druid->priv->username,
				   druid->priv->password);
	gbz_tree_add_account (druid->priv->tree, account);

	gtk_widget_hide (druid->priv->window);

	return FALSE;
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GtkWidget *
gbz_account_druid_new (void)
{
	return GTK_WIDGET (g_object_new (GBZ_TYPE_ACCOUNT_DRUID, NULL));
}

void
gbz_account_druid_new_account (GbzAccountDruid *druid,
			       GbzTree         *tree)
{
	druid->priv->tree = tree;

	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid), FALSE, TRUE,
					   TRUE, TRUE);
	gnome_druid_set_page (GNOME_DRUID (druid), druid->priv->intro_page);
	gnome_druid_construct_with_window (GNOME_DRUID (druid),
					   _("New Account"),
					   NULL, TRUE, &druid->priv->window);
}
