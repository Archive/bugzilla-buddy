/* gnome-bugzilla
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <bonobo/bonobo-i18n.h>
#include <gtk/gtk.h>
#include <glade/glade-xml.h>
#include <libgnome/gnome-macros.h>
#include "query.h"

struct _GbzQueryPrivate {
	GtkWidget *dialog;
	GtkWidget *rule_box;
	GtkWidget *rule_menu;
	GtkWidget *add_button;

	GSList     *options;
};

typedef enum {
	QUERY_TYPE_END,
	QUERY_TYPE_BOOL,
	QUERY_TYPE_TEXT,
	QUERY_TYPE_NUMBER,
	QUERY_TYPE_LIST,
	QUERY_TYPE_STRING,
	QUERY_TYPE_KEYWORD
} QueryType;

typedef struct _QueryItem		QueryItem;
typedef struct _QueryOption		QueryOption;
typedef struct _QueryCallback		QueryCallback;
typedef struct _QueryText		QueryText;

struct _QueryItem {
	QueryType        type;
	char            *desc;
	const QueryText *list;
};

struct _QueryOption {
	int           index;
	gboolean      enabled;
	int           selected_option;

	union {
		char *text;
		int   number;
		int   index;
	} data;
};

struct _QueryCallback {
	GbzQuery     *query;
	QueryOption  *option;
};

struct _QueryText {
	char *label;
	char *value;
};

const QueryText priority_list[] = {
	{ N_("Immediate"), "Immediate" },
	{ N_("Urgent"), "Urgent" },
	{ N_("High"), "High" },
	{ N_("Normal"), "Normal" },
	{ N_("Low"), "Low" },
	{ NULL, NULL }
};

const QueryText severity_list[] = {
	{ N_("Blocker"), "blocker" },
	{ N_("Critical"), "critical" },
	{ N_("Major"), "major" },
	{ N_("Normal"), "normal" },
	{ N_("Minor"), "minor" },
	{ N_("Trivial"), "trivial" },
	{ N_("Enhancement"), "enhancement" },
	{ NULL, NULL }
};

const QueryText status_list[] = {
	{ N_("UNCONFIRMED"), "UNCONFIRMED" },
	{ N_("NEW"), "NEW" },
	{ N_("ASSIGNED"), "ASSIGNED" },
	{ N_("NEEDINFO"), "NEEDINFO" },
	{ N_("REOPENED"), "REOPENED" },
	{ N_("RESOLVED"), "RESOLVED" },
	{ N_("VERIFIED"), "VERIFIED" },
	{ N_("CLOSED"), "CLOSED" },
	{ NULL, NULL }
};

const QueryText resolution_list[] = {
	{ N_("FIXED"), "FIXED" },
	{ N_("WONTFIX"), "WONTFIX" },
	{ N_("DUPLICATE"), "DUPLICATE" },
	{ N_("NOTABUG"), "NOTABUG" },
	{ N_("NOTGNOME"), "NOTGNOME" },
	{ N_("INCOMPLETE"), "INCOMPLETE" },
	{ N_("INVALID"), "INVALID" },
	{ NULL, NULL }
};

const QueryText string_list[] = {
	{ N_("Case-insensitive substring"), "substring" },
	{ N_("Case-sensitive substring"), "casesubstring" },
	{ N_("All words"), "allwords" },
	{ N_("Any words"), "anywords" },
	{ N_("Regular expression"), "regexp" },
	{ N_("Not (regular expression"), "notregexp" },
	{ NULL, NULL }
};

const QueryText keyword_list[] = {
	{ N_("Any of the listed keywords"), "anywords" },
	{ N_("All of the listed keywords"), "allwords" },
	{ N_("None of the listed keywords"), "nowords" },
	{ NULL, NULL }
};

const QueryItem query_item_list[] = {
	{ QUERY_TYPE_TEXT, N_("Product name is"), NULL },
	{ QUERY_TYPE_TEXT, N_("Version is"), NULL },
	{ QUERY_TYPE_TEXT, N_("Component is"), NULL },
	{ QUERY_TYPE_TEXT, N_("Operating system is"), NULL },
	{ QUERY_TYPE_LIST, N_("Priority is"), priority_list },
	{ QUERY_TYPE_LIST, N_("Severity is"), severity_list },
	{ QUERY_TYPE_LIST, N_("Status is"), status_list },
	{ QUERY_TYPE_LIST, N_("Resolution is"), resolution_list },
	{ QUERY_TYPE_STRING, N_("Summary contains"), NULL },
	{ QUERY_TYPE_STRING, N_("Description contains"), NULL },
	{ QUERY_TYPE_STRING, N_("URL contains"), NULL },
	{ QUERY_TYPE_STRING, N_("Whiteboard contains"), NULL },
	{ QUERY_TYPE_KEYWORD, N_("Keywords contains"), NULL },
	{ QUERY_TYPE_END, NULL, NULL }
};

static void gbz_query_finalize (GObject *object);

static void add_button_cb (GtkWidget *widget,
			   gpointer   data);
static void remove_button_cb (GtkWidget *widget,
			      gpointer   data);
static void entry_changed_cb (GtkWidget *widget,
			      gpointer   data);
static void option_menu_changed_cb (GtkWidget *widget,
				    gpointer   data);
static void enabled_button_cb (GtkWidget *widget,
			       gpointer   data);

/* Boilerplate code. */
GNOME_CLASS_BOILERPLATE (GbzQuery, gbz_query,
			 GObject, G_TYPE_OBJECT);

/* Private methods. */
static void
gbz_query_class_init (GbzQueryClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gbz_query_finalize;
}

static void
gbz_query_instance_init (GbzQuery *query)
{
	query->priv = g_new0 (GbzQueryPrivate, 1);
	query->priv->options = NULL;
}

static void
gbz_query_finalize (GObject *object)
{
}

static void
add_button_cb (GtkWidget *widget,
	       gpointer   data)
{
	GbzQuery *query = GBZ_QUERY (data);
	GtkWidget *hbox, *label, *entry, *button, *check, *menu, *menuitem;
	GtkWidget *opt;
	int i, index;
	QueryItem item;
	QueryOption *option;
	QueryCallback *qc;

	hbox = gtk_hbox_new (FALSE, 3);

	index = gtk_option_menu_get_history (GTK_OPTION_MENU (query->priv->rule_menu));
	item = query_item_list[index];
	label = gtk_label_new (item.desc);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	option = g_new (QueryOption, 1);
	option->index = index;
	option->enabled = TRUE;
	option->selected_option = 0;
	query->priv->options = g_slist_append (query->priv->options, option);

	switch (item.type) {
	case QUERY_TYPE_BOOL:
		break;
	case QUERY_TYPE_TEXT:
	case QUERY_TYPE_NUMBER:
		entry = gtk_entry_new ();
		g_signal_connect (G_OBJECT (entry), "changed",
				  G_CALLBACK (entry_changed_cb), option);
		gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
		break;
	case QUERY_TYPE_LIST:
		entry = gtk_option_menu_new ();
		menu = gtk_menu_new ();
		for (i = 0;; i++) {
			if (item.list[i].label == NULL)
				break;
			menuitem = gtk_menu_item_new_with_label (item.list[i].label);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
			gtk_widget_show (menuitem);
		}
		gtk_option_menu_set_menu (GTK_OPTION_MENU (entry), menu);
		gtk_widget_show (menu);
		g_signal_connect (G_OBJECT (entry), "changed",
				  G_CALLBACK (entry_changed_cb), option);
		gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
		option->data.index = 0;
		break;
	case QUERY_TYPE_STRING:
		entry = gtk_entry_new ();
		opt = gtk_option_menu_new ();
		menu = gtk_menu_new ();
		for (i = 0;; i++) {
			if (string_list[i].label == NULL)
				break;
			menuitem = gtk_menu_item_new_with_label (string_list[i].label);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		}
		gtk_option_menu_set_menu (GTK_OPTION_MENU (opt), menu);
		gtk_widget_show (menu);
		g_signal_connect (G_OBJECT (entry), "changed",
				  G_CALLBACK (entry_changed_cb), option);
		g_signal_connect (G_OBJECT (opt), "changed",
				  G_CALLBACK (option_menu_changed_cb), option);
		gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
		gtk_box_pack_start (GTK_BOX (hbox), opt, FALSE, FALSE, 0);
		break;
	case QUERY_TYPE_KEYWORD:
		entry = gtk_entry_new ();
		opt = gtk_option_menu_new ();
		menu = gtk_menu_new ();
		for (i = 0;; i++) {
			if (keyword_list[i].label == NULL)
				break;
			menuitem = gtk_menu_item_new_with_label (keyword_list[i].label);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		}
		gtk_option_menu_set_menu (GTK_OPTION_MENU (opt), menu);
		gtk_widget_show (menu);
		g_signal_connect (G_OBJECT (entry), "changed",
				  G_CALLBACK (entry_changed_cb), option);
		g_signal_connect (G_OBJECT (opt), "changed",
				  G_CALLBACK (option_menu_changed_cb), option);
		gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
		gtk_box_pack_start (GTK_BOX (hbox), opt, FALSE, FALSE, 0);
		option->data.index = 0;
		break;
	default:
		break;
	}

	check = gtk_check_button_new_with_label (_("Enabled"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), TRUE);
	g_signal_connect (G_OBJECT (check), "toggled",
			  G_CALLBACK (enabled_button_cb), option);
	gtk_box_pack_start (GTK_BOX (hbox), check, FALSE, FALSE, 0);

	button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	qc = g_new (QueryCallback, 1);
	qc->query = query;
	qc->option = option;
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (remove_button_cb), qc);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX (query->priv->rule_box), hbox, FALSE, FALSE, 0);
	gtk_widget_show_all (hbox);
}

static void
remove_button_cb (GtkWidget *widget,
		  gpointer   data)
{
	QueryCallback *qc = data;

	gtk_container_remove (GTK_CONTAINER (qc->query->priv->rule_box), widget->parent);
	qc->query->priv->options = g_slist_remove (qc->query->priv->options,
						   qc->option);
	g_free (qc);
}

static void
entry_changed_cb (GtkWidget *widget,
		  gpointer   data)
{
	QueryOption *option = data;

	switch (query_item_list[option->index].type) {
	case QUERY_TYPE_TEXT:
	case QUERY_TYPE_STRING:
	case QUERY_TYPE_KEYWORD:
		option->data.text = g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));
		break;
	case QUERY_TYPE_NUMBER:
		sscanf (gtk_entry_get_text (GTK_ENTRY (widget)),
			"%d", &option->data.number);
		break;
	case QUERY_TYPE_LIST:
		option->data.index = gtk_option_menu_get_history (GTK_OPTION_MENU (widget));
		break;
	default:
		break;
	}
}

static void
option_menu_changed_cb (GtkWidget *widget,
			gpointer   data)
{
	QueryOption *option = data;

	option->selected_option = gtk_option_menu_get_history (GTK_OPTION_MENU (widget));
}

static void
enabled_button_cb (GtkWidget *widget,
		   gpointer   data)
{
	QueryOption *option = data;
	option->enabled = !option->enabled;
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

GbzQuery *
gbz_query_new (void)
{
	GbzQuery *query;

	query = GBZ_QUERY (g_object_new (GBZ_QUERY_TYPE, NULL));

	return query;
}

GtkResponseType
gbz_query_show_dialog (GbzQuery *query)
{
	GbzQueryPrivate *priv;
	GladeXML *gui;
	GtkWidget *menu, *menuitem;
	QueryItem item;
	int i;
	GdkGeometry hints;
	GtkResponseType response;

	g_return_val_if_fail (query != NULL, -1);
	g_return_val_if_fail (GBZ_IS_QUERY (query), -1);

	priv = query->priv;

	/* Open gbz-view.glade. */
	gui = glade_xml_new (DATADIR "/bugzilla-buddy/glade/view.glade",
			     "querydialog", NULL);
	if (!gui) {
		g_warning ("Could not find view.glade, reinstall bugzilla-buddy.\n");
		return 0;
	}

	/* Get widgets. */
	priv->dialog     = glade_xml_get_widget (gui, "querydialog");
	priv->rule_box   = glade_xml_get_widget (gui, "rulebox");
	priv->rule_menu  = glade_xml_get_widget (gui, "rulemenu");
	priv->add_button = glade_xml_get_widget (gui, "addbutton");

	/* Add query options to optionmenu. */
	menu = gtk_menu_new ();
	for (i = 0;; i++) {
		item = query_item_list[i];
		if (item.type == QUERY_TYPE_END)
			break;
		menuitem = gtk_menu_item_new_with_label (g_strdup (item.desc));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		gtk_widget_show (menuitem);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (priv->rule_menu), menu);
	gtk_widget_show (menu);

	g_signal_connect (G_OBJECT (priv->add_button),
			  "clicked",
			  G_CALLBACK (add_button_cb),
			  query);

	hints.min_width = 500;
	hints.min_height = 250;
	gtk_window_set_geometry_hints (GTK_WINDOW (priv->dialog), priv->dialog,
				       &hints, GDK_HINT_MIN_SIZE);

	response = gtk_dialog_run (GTK_DIALOG (priv->dialog));
	gtk_widget_hide (priv->dialog);

	return response;
}

#if 0
GNOME_Bugzilla_QueryObject *
gbz_query_get_query_object (GbzQuery *query)
{
	GNOME_Bugzilla_QueryObject *query_obj;
	GSList *l;
	int max;

	max = g_slist_length (query->priv->options);

	/* Create new QueryObject. */
	query_obj = GNOME_Bugzilla_QueryObject__alloc ();
	query_obj->product._maximum = max;
	query_obj->product._length = 0;
	query_obj->product._buffer = GNOME_Bugzilla_StringList_allocbuf (max);
	query_obj->version._maximum = max;
	query_obj->version._length = 0;
	query_obj->version._buffer = GNOME_Bugzilla_StringList_allocbuf (max);
	query_obj->component._maximum = max;
	query_obj->component._length = 0;
	query_obj->component._buffer = GNOME_Bugzilla_StringList_allocbuf (max);
	query_obj->op_sys._maximum = max;
	query_obj->op_sys._length = 0;
	query_obj->op_sys._buffer = GNOME_Bugzilla_StringList_allocbuf (max);
	query_obj->priority._maximum = max;
	query_obj->priority._length = 0;
	query_obj->priority._buffer = GNOME_Bugzilla_StringList_allocbuf (max);
	query_obj->severity._maximum = max;
	query_obj->severity._length = 0;
	query_obj->severity._buffer = GNOME_Bugzilla_StringList_allocbuf (max);
	query_obj->status._maximum = max;
	query_obj->status._length = 0;
	query_obj->status._buffer = GNOME_Bugzilla_StringList_allocbuf (max);
	query_obj->resolution._maximum = max;
	query_obj->resolution._length = 0;
	query_obj->resolution._buffer = GNOME_Bugzilla_StringList_allocbuf (max);

	/* Initialize string values to empty strings. */
	query_obj->summary = CORBA_string_dup ("");
	query_obj->summary_type = CORBA_string_dup ("");
	query_obj->description = CORBA_string_dup ("");
	query_obj->description_type = CORBA_string_dup ("");
	query_obj->url = CORBA_string_dup ("");
	query_obj->url_type = CORBA_string_dup ("");
	query_obj->whiteboard = CORBA_string_dup ("");
	query_obj->whiteboard_type = CORBA_string_dup ("");
	query_obj->keywords = CORBA_string_dup ("");
	query_obj->keywords_type = CORBA_string_dup ("");

	/* Fill QueryObject with values. */
	for (l = query->priv->options; l != NULL; l = l->next) {
		QueryOption *option = l->data;
		switch (option->index) {
		case 0: /* Product */
			query_obj->product._buffer[query_obj->product._length++] = 
				CORBA_string_dup (option->data.text);
			break;
		case 1: /* Version */
			query_obj->version._buffer[query_obj->version._length++] = 
				CORBA_string_dup (option->data.text);
			break;
		case 2: /* Component */
			query_obj->component._buffer[query_obj->component._length++] = 
				CORBA_string_dup (option->data.text);
			break;
		case 3: /* Operating system */
			query_obj->op_sys._buffer[query_obj->op_sys._length++] = 
				CORBA_string_dup (option->data.text);
			break;
		case 4: /* Priority */
			query_obj->priority._buffer[query_obj->priority._length++] = 
				CORBA_string_dup (priority_list[option->data.index].value);
			break;
		case 5: /* Severity */
			query_obj->severity._buffer[query_obj->severity._length++] = 
				CORBA_string_dup (severity_list[option->data.index].value);
			break;
		case 6: /* Status */
			query_obj->status._buffer[query_obj->status._length++] = 
				CORBA_string_dup (status_list[option->data.index].value);
			break;
		case 7: /* Resolution */
			query_obj->resolution._buffer[query_obj->resolution._length++] = 
				CORBA_string_dup (resolution_list[option->data.index].value);
			break;
		case 8: /* Summary */
			query_obj->summary = CORBA_string_dup (option->data.text);
			query_obj->summary_type = CORBA_string_dup (string_list[option->selected_option].value);
			break;
		case 9: /* Description */
			query_obj->description = CORBA_string_dup (option->data.text);
			query_obj->description_type = CORBA_string_dup (string_list[option->selected_option].value);
			break;
		case 10: /* URL */
			query_obj->url = CORBA_string_dup (option->data.text);
			query_obj->url_type = CORBA_string_dup (string_list[option->selected_option].value);
			break;
		case 11: /* Whiteboard */
			query_obj->whiteboard = CORBA_string_dup (option->data.text);
			query_obj->whiteboard_type = CORBA_string_dup (string_list[option->selected_option].value);
			break;
		case 12: /* Keywords */
			query_obj->keywords = CORBA_string_dup (option->data.text);
			query_obj->keywords_type = CORBA_string_dup (keyword_list[option->selected_option].value);
			break;
		default:
			g_warning ("Unknown option type\n");
			break;
		}
	}

	return query_obj;
}
#endif
