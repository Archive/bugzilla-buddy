/* bugzilla-buddy
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GBZ_PREFS_H
#define GBZ_PREFS_H

#define GBZ_BASE_KEY			"/apps/bugzilla-buddy/"
#define GBZ_SERVER_KEY			"bugzilla_server"
#define GBZ_USERNAME_KEY		"username"
#define GBZ_PASSWORD_KEY		"password"

void gbz_show_prefs (void);

#endif /* GBZ_PREFS_H */
